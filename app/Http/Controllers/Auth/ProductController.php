<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Response;
use Validator;
use Config;
use Auth;

class ProductController extends Controller
{

    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'                      => 'required',
            'type'                   => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model                      = new Product;
        $model->name                = $request->input('name');
        if($request->input('description',null) !== null) $model->description = $request->input('description');
        $model->type_id             = $request->input('type');
        $model->created_by          = Auth::user()->id;
        $model->save();

        // return redirect('../../../../admin/products/list');

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required',
            'type_id'       => 'integer'        

        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = Product::find($request->input('id'));
 
        if($request->input('name',null) !== null) $model->name = $request->input('name');
        if($request->input('description',null) !== null) $model->description = $request->input('description');
        if($request->input('type_id',null) !== null) $model->type_id = $request->input('type_id');

        $model->save();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

}
