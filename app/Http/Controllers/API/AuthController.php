<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Validator;
use Mail;
use Redirect;
use View;
use Response;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
// use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;


class AuthController extends Controller
{

    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'loggedin'              => 'required',
            'password'              => 'required|min:6'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        //INITIALIZATION
        $input = $request->all();
        
        $emailUser = $input['loggedin'];
        $password = $input['password'];

        // PARAMETERS FOR AUTHENTICATION
        $userdata1 = array(
            'email'     => $emailUser,
            'password'  => $password,
        );

        $userdata2 = array(
            'username'     => $emailUser,
            'password'  => $password,
        );
        
        // AUTHENTICATE USER
        if (Auth::attempt($userdata1) == true) 
        {
            $user = User::find(Auth::user()->id);
            return redirect('/admin');
            // return Response::json(array('result' => true, 'data' => $user, 'message' => 'succesfully signed in' ) );
            
        }
        else if (Auth::attempt($userdata2) == true) 
        {
            $user = User::find(Auth::user()->id);
            return redirect('/admin');
            // return Response::json(array('result' => true, 'data' => $user, 'message' => 'succesfully signed in' ) );
            
        }
        else
        {
            Auth::logout();
            return redirect('/');
            // return Response::json(array('result' => false ,'message' => 'incorrect email / password' ) );
        }

    }

    protected function delete(Request $request)
    {
        Auth::logout();
        return redirect('/');
        // return Response::json(array('result' => true ,'message' => 'succesfully sign out' ) );
    }

}
