<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\PersonalInformation;
use App\Models\GovermentInformation;
use Response;
use Validator;
use Config;

class UserController extends Controller
{
    protected function show(Request $request)
    {

        //INITIALIZATION
        $input = $request->all();

        $sort = $request->input('sort', 'asc');
        $orderBy = $request->input('order', 'created_at');
        $model = null;

        if( $sort != 'asc' && $sort != 'desc' )
        {
             $sort = 'asc';
        }

        if ( $request->has('key') && $request->has('filter') )
        {
            switch ($request->input('filter') ) {
                case 'firstname':
                    $model = User::where('firstname', 'like', '%'.$request->input('key').'%')
                        ->with('Type','PersonalInformation','GovermentInformation')
                        ->orderBy($orderBy, $sort)
                        ->get();
                    break;

                case 'lastname':
                    $model = User::where('lastname', 'like', '%'.$request->input('key').'%')
                        ->with('Type','PersonalInformation','GovermentInformation')
                        ->orderBy($orderBy, $sort)
                        ->get();
                    break;

                case 'username':
                    $model = User::where('username', 'like', '%'.$request->input('key').'%')
                        ->with('Type','PersonalInformation','GovermentInformation')
                        ->orderBy($orderBy, $sort)
                        ->get();
                    break;

                case 'email':
                    $model = User::where('email', 'like', '%'.$request->input('key').'%')
                        ->with('Type','PersonalInformation','GovermentInformation')
                        ->orderBy($orderBy, $sort)
                        ->get();
                    break;

                case 'type':
                    $model = User::where('type', $request->input('key'))
                        ->with('Type','PersonalInformation','GovermentInformation')
                        ->orderBy($orderBy, $sort)
                        ->get();
                    break;
                
                default:
                    $model = User::where('username', 'like', '%'.$request->input('key').'%')
                        ->with('Type','PersonalInformation','GovermentInformation')
                        ->orWhere('firstname', 'like', '%'.$request->input('key').'%')
                        ->orWhere('lastname', 'like', '%'.$request->input('key').'%')
                        ->orWhere('middlename', 'like', '%'.$request->input('key').'%')
                        ->orWhere('email', 'like', '%'.$request->input('key').'%')
                        ->orderBy($orderBy, $sort)
                        ->get();
                    break;
            }
        }
        else if ( $request->has('key') )
        {
            $model = User::where('username', 'like', '%'.$request->input('key').'%')
                        ->orWhere('firstname', 'like', '%'.$request->input('key').'%')
                        ->orWhere('lastname', 'like', '%'.$request->input('key').'%')
                        ->orWhere('middlename', 'like', '%'.$request->input('key').'%')
                        ->orWhere('email', 'like', '%'.$request->input('key').'%')
                        ->orWhere('type', $request->input('key'))
                        ->orderBy($orderBy, $sort)
                        ->get();
        }
        else if  ( $request->has('id') )
        {
            $model = User::with('Type','PersonalInformation','GovermentInformation')
                        ->where('id',$request->input('id'))
                        ->first();
        }
        else
        {
            $model = User::orderBy($orderBy, $sort)
                        ->with('Type','PersonalInformation','GovermentInformation')
                        ->get();
        }

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'              => 'required|max:255|unique:users',
            'email'                 => 'required|email|max:255|unique:users',
            'position'              => 'required',
            'firstname'             => 'required',
            'lastname'              => 'required',
            'password'              => 'required|min:6',
            'password_confirmation' => 'required|min:6|same:password',
            'type'                  => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = new User;
        $model->username        = $request->input('username');
        $model->email           = $request->input('email');
        $model->position        = $request->input('position');
        $model->firstname       = $request->input('firstname');
        if($request->input('middlename',null) !== null) $model->middlename = $request->input('middlename');
        $model->lastname        = $request->input('lastname');
        $model->type            = $request->input('type');
        $model->password        = bcrypt($request->input('passwaord'));
        $model->save();

        $result = array('result' => true , 'message' => 'Insert Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function createComplete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'              => 'required|max:255|unique:users',
            'email'                 => 'required|email|max:255|unique:users',
            'firstname'             => 'required',
            'lastname'              => 'required',
            'type'                  => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = new User;
        $model->username        = $request->input('username');
        $model->email           = $request->input('email');
        $model->position        = $request->input('position');
        if($request->input('position',null) !== null)       $model->position    = $request->input('position');
        $model->firstname       = $request->input('firstname');
        if($request->input('middlename',null) !== null)     $model->middlename  = $request->input('middlename');
        $model->lastname        = $request->input('lastname');
        $model->type            = $request->input('type');
        $model->password        = bcrypt($request->input('lastname').'123');
        $model->save();

        if ($model){
            $personalModel = new PersonalInformation;
            $personalModel->user_id                         = $model->id;
            if($request->input('birthday',null)             !== null) $personalModel->birthday              = date('Y-m-d', strtotime($request->input('birthday')) );
            if($request->input('age',null)                  !== null) $personalModel->age                   = $request->input('age');
            if($request->input('religion',null)             !== null) $personalModel->religion              = $request->input('religion');
            if($request->input('nationality',null)          !== null) $personalModel->nationality           = $request->input('nationality');
            if($request->input('birth_place',null)          !== null) $personalModel->birth_place           = $request->input('birth_place');
            if($request->input('father_lastname',null)      !== null) $personalModel->father_lastname       = $request->input('father_lastname');
            if($request->input('father_firstname',null)     !== null) $personalModel->father_firstname      = $request->input('father_firstname');
            if($request->input('father_middlename',null)    !== null) $personalModel->father_middlename     = $request->input('father_middlename');
            if($request->input('mother_lastname',null)      !== null) $personalModel->mother_lastname       = $request->input('mother_lastname');
            if($request->input('mother_firstname',null)     !== null) $personalModel->mother_firstname      = $request->input('mother_firstname');
            if($request->input('mother_middlename',null)    !== null) $personalModel->mother_middlename     = $request->input('mother_middlename'); 
            if($request->input('address1',null)             !== null) $personalModel->address1              = $request->input('address1'); 
            if($request->input('address2',null)             !== null) $personalModel->address2              = $request->input('address2'); 
            if($request->input('address3',null)             !== null) $personalModel->address3              = $request->input('address3');  
            if($request->input('city',null)                 !== null) $personalModel->city                  = $request->input('city'); 
            if($request->input('region',null)               !== null) $personalModel->region                = $request->input('region'); 
            if($request->input('country',null)              !== null) $personalModel->country               = $request->input('country'); 

            $personalModel->save();           
        }

        if ($model){
            $govermentModel = new GovermentInformation;
            $govermentModel->user_id            = $model->id;
            if($request->input('TIN',null)      !== null) $govermentModel->TIN       = $request->input('TIN');
            if($request->input('SSS',null)      !== null) $govermentModel->SSS       = $request->input('SSS');
            if($request->input('PAGIBIG',null)  !== null) $govermentModel->PAGIBIG   = $request->input('PAGIBIG');
            $govermentModel->save();           
        }

        $result = array('result' => true , 'message' => 'Insert Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'                  => 'required',
            'type'                => 'integer'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = User::find($request->input('id'));
 
        if($request->input('username',null)     !== null) $model->username      = $request->input('username');
        if($request->input('email',null)        !== null) $model->email         = $request->input('email');
        if($request->input('position',null)     !== null) $model->position      = $request->input('position');
        if($request->input('firstname',null)    !== null) $model->firstname     = $request->input('firstname');
        if($request->input('middlename',null)   !== null) $model->middlename    = $request->input('middlename');
        if($request->input('lastname',null)     !== null) $model->lastname      = $request->input('lastname');
        if($request->input('type',null)         !== null) $model->type          = $request->input('type');
        if($request->input('isActive',null)     !== null) $model->isActive      = $request->input('isActive');

        $model->save();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'                    => 'required',
            'password'              => 'required|min:6',
            'password_confirmation' => 'required|min:6|same:password'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = User::find($request->input('id'));
        $model->password        = bcrypt($request->input('passwaord'));
        $model->save();

        $result = array('result' => true , 'message' => 'Password Update Success');
        return Response::json( $result );

    }

}
