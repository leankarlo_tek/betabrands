<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PersonalInformation;
use Response;
use Validator;
use Config;

class PersonalInfoController extends Controller
{
    protected function show(Request $request)
    {
        $input = $request->all();

        $sort = $request->input('sort', 'desc');
        $orderBy = $request->input('order', 'created_at');
        $model = null;

        if( $sort != 'asc' && $sort != 'desc' )
        {
             $sort = 'asc';
        }

        if($request->has('id'))
        {
            $model = PersonalInformation::find($request->input('id'));
        }
        else
        {
            $model = PersonalInformation::orderBy($orderBy, $sort)->get();
        }

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'                  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model                      = new PersonalInformation;
        $model->user_id                = $request->input('user_id');
        
        if($request->input('birthday',null) !== null)           $model->birthday            = date('Y-m-d', strtotime($request->input('birthday')) );
        if($request->input('age',null) !== null)                $model->age                 = $request->input('age');
        if($request->input('religion',null) !== null)           $model->religion            = $request->input('religion');
        if($request->input('nationality',null) !== null)        $model->nationality         = $request->input('nationality');
        if($request->input('birth_place',null) !== null)        $model->birth_place         = $request->input('birth_place');
        if($request->input('father_lastname',null) !== null)    $model->father_lastname     = $request->input('father_lastname');
        if($request->input('father_firstname',null) !== null)   $model->father_firstname    = $request->input('father_firstname');
        if($request->input('father_middlename',null) !== null)  $model->father_middlename   = $request->input('father_middlename');
        if($request->input('mother_lastname',null) !== null)    $model->mother_lastname     = $request->input('mother_lastname');
        if($request->input('mother_middlename',null) !== null)  $model->mother_middlename   = $request->input('mother_middlename');
        if($request->input('mother_firstname',null) !== null)   $model->mother_firstname    = $request->input('mother_firstname');
        if($request->input('address1',null) !== null)           $model->address1            = $request->input('address1');
        if($request->input('address2',null) !== null)           $model->address2            = $request->input('address2');
        if($request->input('address3',null) !== null)           $model->address3            = $request->input('address3');
        if($request->input('city',null) !== null)               $model->city                = $request->input('city');
        if($request->input('region',null) !== null)             $model->region              = $request->input('region');
        if($request->input('country',null) !== null)            $model->country             = $request->input('country');

        $model->save();

        $result = array('result' => true , 'message' => 'Insert Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function update(Request $request)
    {
        if ( $request->has('id') )
        {
            $model = PersonalInformation::find($request->input('id'));

            if(!$model)
            {
                $result = array('result' => false , 'message' => "Can't find data");
                return Response::json( $result );
            }
        }
        else if ( $request->has('user_id') )
        {
            $model = PersonalInformation::where( 'user_id', $request->input('user_id') )->first();
            if(!$model){
                $model = new PersonalInformation;
                $model->user_id = $request->input('user_id') ;
            }
        }
        else
        {
             return response()->json(array('result' => false, 'message' => 'id or user_id should exist', 400));
        }
        
 
        if($request->input('birthday',null) !== null)           $model->birthday            = date('Y-m-d', strtotime($request->input('birthday')) );
        if($request->input('age',null) !== null)                $model->age                 = $request->input('age');
        if($request->input('religion',null) !== null)           $model->religion            = $request->input('religion');
        if($request->input('nationality',null) !== null)        $model->nationality         = $request->input('nationality');
        if($request->input('birth_place',null) !== null)        $model->birth_place         = $request->input('birth_place');
        if($request->input('father_lastname',null) !== null)    $model->father_lastname     = $request->input('father_lastname');
        if($request->input('father_firstname',null) !== null)   $model->father_firstname    = $request->input('father_firstname');
        if($request->input('father_middlename',null) !== null)  $model->father_middlename   = $request->input('father_middlename');
        if($request->input('mother_lastname',null) !== null)    $model->mother_lastname     = $request->input('mother_lastname');
        if($request->input('mother_middlename',null) !== null)  $model->mother_middlename   = $request->input('mother_middlename');
        if($request->input('mother_firstname',null) !== null)   $model->mother_firstname    = $request->input('mother_firstname');
        if($request->input('address1',null) !== null)           $model->address1            = $request->input('address1');
        if($request->input('address2',null) !== null)           $model->address2            = $request->input('address2');
        if($request->input('address3',null) !== null)           $model->address3            = $request->input('address3');
        if($request->input('city',null) !== null)               $model->city                = $request->input('city');
        if($request->input('region',null) !== null)             $model->region              = $request->input('region');
        if($request->input('country',null) !== null)            $model->country             = $request->input('country');

        $model->save();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

}
