<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JobOrderDevelopment;
use Response;
use Validator;
use Config;

class JobOrderDevelopmentController extends Controller
{
    protected function show(Request $request)
    {
        $model = null;
        if($request->has('id'))
        {
            $model = JobOrderDevelopment::find($request->input('id'));
        }
        else if($request->has('jo_id'))
        {
            $model = JobOrderDevelopment::all();
        }

        if($model == null)
        {
            $result = array('result' => false,'message' => 'Job order development cannot be found!');
            $result = array_add($result, 'data' , $model);
            return Response::json( $result );
        }

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jo_detail_id'                  => 'required|integer',
            'barcode'                       => 'required|integer', /* 4 digit JO NUMBER + batch number + SIZE + jo_detail_id 3 digits + operation step number 2 (digits) */
            'product_operation_id'          => 'required|integer',
            'status'                        => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model                                                                              = new JobOrderDevelopment;
        $model->jo_detail_id                                                                = $request->input('jo_detail_id');
        $model->barcode                                                                     = $request->input('barcode');
        $model->product_operation_id                                                        = $request->input('product_operation_id');
        if($request->input('start',null)            !== null) $model->start                 = $request->input('start');
        if($request->input('end',null)              !== null) $model->end                   = $request->input('end');
        if($request->input('user_id',null)          !== null) $model->user_id               = $request->input('user_id');
        if($request->input('isFinished',null)       !== null) $model->isFinished            = $request->input('isFinished');
        $model->created_by                                                                  = Auth::user()->id;
        $model->status                                                                      = $request->input('status');
        $model->save();

        $result = array('result' => true , 'message' => 'Insert Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required'  

        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = JobOrderDevelopment::find($request->input('id'));
 
        if($request->input('jo_detail_id',null)         !== null) $model->jo_detail_id          = $request->input('jo_detail_id');
        if($request->input('barcode',null)              !== null) $model->barcode               = $request->input('barcode');
        if($request->input('product_operation_id',null) !== null) $model->product_operation_id  = $request->input('product_operation_id');
        if($request->input('start',null)                !== null) $model->start                 = $request->input('start');
        if($request->input('end',null)                  !== null) $model->end                   = $request->input('end');
        if($request->input('user_id',null)              !== null) $model->user_id               = $request->input('user_id');
        if($request->input('isFinished',null)           !== null) $model->isFinished            = $request->input('isFinished');
        if($request->input('status',null)               !== null) $model->status                = $request->input('status');

        $model->save();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required'  

        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = JobOrderDevelopment::find($request->input('id'));
        $model->delete();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

}
