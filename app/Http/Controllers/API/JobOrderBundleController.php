<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JobOrderBundles;
use App\Models\JobOrder;
use App\Models\Product;
use App\Models\JobOrderDetail;
use App\Models\ProductOperation;
use App\Models\JobOrderDevelopment;
use App\Models\JobOrderBundleOperations;
use App\Models\ProductSize;
use Response;
use Validator;
use Config;
use Auth;

class JobOrderBundleController extends Controller
{
	protected function show(Request $request)
	{
		$model = null;
		if($request->has('id'))
		{
			$model = JobOrderBundles::where('id',$request->input('id'))->with('Product','Color','Size','Status')->first();
		}
		else if($request->has('jo_detail_id'))
		{
			$model = JobOrderBundles::where('jo_detail_id',$request->input('jo_detail_id'))->with('Product','Color','Size','Status')->get();
		}

		if($model == null)
		{
			$result = array('result' => false,'message' => 'Job order details cannot be found!');
			$result = array_add($result, 'data' , $model);
			return Response::json( $result );
		}

		$result = array('result' => true);
		$result = array_add($result, 'data' , $model);
		return Response::json( $result );

	}

	protected function create(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'jo_detail_id'         => 'required|integer'
		]);

		if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		// Check if there is already an existing batch. If there is an existing batch, count the batch
		// then add 1 for the batch number
		$batchCount = 1;

		$modelCount = JobOrderBundles::where('jo_detail_id',$request->input('jo_detail_id'))->get();

		if ($modelCount->count() != 0) {
			$batchCount = $modelCount->count() + 1;
		}

		$model                          			= new JobOrderBundles;
		
		$model->jo_detail_id                    	= $request->input('jo_detail_id');
		if($request->input('color_id',null)         !== null) $model->color_id          = $request->input('color_id');
		if($request->input('size_id',null)          !== null) $model->size_id           = $request->input('size_id');  
		if($request->input('bundle_quantity',null)  !== null) $model->bundle_quantity   = $request->input('bundle_quantity');
		if($request->input('started_on',null)       !== null) $model->started_on        = $request->input('started_on');
		if($request->input('ended_on',null)         !== null) $model->ended_on          = $request->input('ended_on');
		if($request->input('status',null)           !== null) $model->status            = $request->input('status');
		
		$model->save();

		$result = array('result' => true , 'message' => 'Insert Success');
		$result = array_add($result, 'data' , $model);
		return Response::json( $result );

	}

	protected function createWithOperations(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'jo_detail_id'         	=> 'required|integer',
			'bundle_quantity'		=> 'required|integer',
			'color_id'				=> 'required|integer',
			'size_id'				=> 'required|integer'
		]);

		if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		$batchModel 			= JobOrderDetail::find( $request->input('jo_detail_id') );
		$joModel 				= JobOrder::find($batchModel->jo_id);
		$sizeModel 				= ProductSize::find( $request->input('size_id') );
		
		$productModel 			= Product::where('id', $batchModel->product_id )
                            	->with('Operations','Type')
                            	->first();
        
        $remainingVolume 		= 0;
        $typeBundleSize 		= $productModel->type->bundle_size; 
        $bundleVolume 			= $request->input('bundle_quantity'); 
        $volumePerBundle 		= floor($bundleVolume / $typeBundleSize); 

        $xHolder = $typeBundleSize * $volumePerBundle; 
        $remainingVolume = $bundleVolume - $xHolder; 
        
		if ($productModel->operations->count()) 
		{
			$bundleCount = 1;
			for($i = 0; $i<$volumePerBundle ; $i++ )
			{
				$model                          			= new JobOrderBundles;
				$model->jo_detail_id                    	= $request->input('jo_detail_id');
				$model->bundle_quantity   					= $typeBundleSize;
				$model->bundle_number						= $bundleCount;
				if($request->input('color_id',null)         !== null) $model->color_id          = $request->input('color_id');
				if($request->input('size_id',null)          !== null) $model->size_id           = $request->input('size_id');
				if($request->input('started_on',null)       !== null) $model->started_on        = $request->input('started_on');
				if($request->input('ended_on',null)         !== null) $model->ended_on          = $request->input('ended_on');
				$model->status            					= $request->input('status',2);
				$model->save();
				if($model)
				{
					$ticketCount = 1;
					foreach ($productModel->operations as $value)
					{
						$bundleOperation 					= new JobOrderBundleOperations;
						$bundleOperation->bundle_id 		= $model->id;
						$bundleOperation->operation_id 		= $value->id;
						$bundleOperation->status 			= 2;
						$bundleOperation->operation_code 	= $joModel->c_jo . ' ' . $sizeModel->name . ' ' . str_pad($bundleCount, 3, "0", STR_PAD_LEFT) . ' ' . str_pad($ticketCount, 2, "0", STR_PAD_LEFT);
						$ticketCount++;
						$bundleOperation->save();
					}
					if(!$bundleOperation)
					{
						$model->delete();
					}
				}
				$bundleCount++;
			}
			if( $remainingVolume != 0 )
			{
				$model                          			= new JobOrderBundles;
				$model->jo_detail_id                    	= $request->input('jo_detail_id');
				$model->bundle_quantity   					= $remainingVolume;
				if($request->input('color_id',null)         !== null) $model->color_id          = $request->input('color_id');
				if($request->input('size_id',null)          !== null) $model->size_id           = $request->input('size_id');
				if($request->input('started_on',null)       !== null) $model->started_on        = $request->input('started_on');
				if($request->input('ended_on',null)         !== null) $model->ended_on          = $request->input('ended_on');
				$model->status            = $request->input('status',2);
				$model->save();
				if($model)
				{
					foreach ($productModel->operations as $value)
					{
						$bundleOperation 					= new JobOrderBundleOperations;
						$bundleOperation->bundle_id 		= $model->id;
						$bundleOperation->operation_id 		= $value->id;
						$bundleOperation->status 			= 2;
						
						$bundleOperation->save();
					}
					if(!$bundleOperation)
					{
						$model->delete();
					}
				}	
			}
		}
		else
		{
			$result = array('result' => false , 'message' => 'The product still has no operations. Please add operations first on the product');
			return Response::json( $result );
		}
		
		$result = array('result' => true , 'message' => 'Bundle Has Been Created!!');
		$result = array_add($result, 'data' , $model);
		return Response::json( $result );

	}

	protected function update(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'id'            => 'required'  

		]);

		if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		$model = JobOrderBundles::find($request->input('id'));
 
		if($request->input('jo_detail_id',null)     !== null) $model->jo_detail_id      = $request->input('jo_detail_id');
		if($request->input('product_id',null)       !== null) $model->product_id        = $request->input('product_id');
		if($request->input('color_id',null)         !== null) $model->color_id          = $request->input('color_id');
		if($request->input('size_id',null)          !== null) $model->size_id           = $request->input('size_id');  
		if($request->input('bundle_quantity',null)  !== null) $model->bundle_quantity   = $request->input('bundle_quantity');
		if($request->input('started_on',null)       !== null) $model->started_on        = $request->input('started_on');
		if($request->input('ended_on',null)         !== null) $model->ended_on          = $request->input('ended_on');
		if($request->input('status',null)           !== null) $model->status            = $request->input('status');
		
		$model->save();

		$result = array('result' => true , 'message' => 'Update Success');
		$result = array_add($result, 'data' , $model);
		return Response::json( $result );

	}

	protected function delete(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'id'            => 'required'  
		]);

		if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		$model = JobOrderBundles::find($request->input('id'));
		$model->delete();

		$result = array('result' => true , 'message' => 'Delete Success');
		$result = array_add($result, 'data' , $model);
		return Response::json( $result );

	}

}
