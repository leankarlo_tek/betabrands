<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JobOrder;
use App\Models\JobOrderDetail;
use App\Models\ProductOperation;
use Response;
use Validator;
use Config;
use Auth;

class JobOrderDetailController extends Controller
{
    protected function show(Request $request)
    {
        $model = null;
        if($request->has('id'))
        {
            $model = JobOrderDetail::where('id',$request->input('id'))->with('Product','Status')->first();
        }
        else if($request->has('jo_id'))
        {
            $model = JobOrderDetail::where('jo_id',$request->input('jo_id'))->with('Product','Status')->get();
        }

        if($model == null)
        {
            $result = array('result' => false,'message' => 'Job order details cannot be found!');
            $result = array_add($result, 'data' , $model);
            return Response::json( $result );
        }

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jo_id'         => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        //check first if the product has an operation
        $operationModel = ProductOperation::where( 'product_id',$request->input('product_id') );

        if($operationModel->count() ){
            // Check if there is already an existing batch. If there is an existing batch, count the batch
            // then add 1 for the batch number
            $batchCount = 1;
    
            $batchModel = JobOrderDetail::where('jo_id',$request->input('jo_id'))->get();
            $jobOrderModel = JobOrder::find($request->input('jo_id'));
    
            if ($batchModel->count() != 0) {
                $batchCount = $batchModel->count() + 1;
            }

            $sum = $batchModel->sum( 'batch_volume' );
            $sum = $sum + $request->input('batch_volume');
            
            if($sum > $jobOrderModel->volume){
                $result = array('result' => false , 'message' => 'Your are exceding the total volume.');
                return Response::json( $result );
            }
            

            $model                          = new JobOrderDetail;
            $model->jo_id                   = $request->input('jo_id');
            $model->batch                   = $batchCount;
            if($request->input('batch_code',null)   !== null)   $model->batch_code      = $request->input('batch_code');
            if($request->input('batch_volume',null)  !== null)  $model->batch_volume    = $request->input('batch_volume');
            if($request->input('product_id',null)   !== null)   $model->product_id      = $request->input('product_id');
    
            $model->status                  = 2;
            $model->created_by              = Auth::user()->id;
            
            $model->save();
        }
        else
        {
            $result = array('result' => false , 'message' => 'The product still has no operations. Please add operations first on the product');
            return Response::json( $result );
        }

        $result = array('result' => true , 'message' => 'Insert Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required'  

        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = JobOrderDetail::find($request->input('id'));
 
        if($request->input('jo_id',null) !== null)          $model->jo_id           = $request->input('jo_id');
        if($request->input('batch',null) !== null)          $model->batch           = $request->input('batch');
        if($request->input('batch_code',null) !== null)     $model->batch_code      = $request->input('batch_code');
        if($request->input('batch_volume',null)  !== null)  $model->batch_volume    = $request->input('batch_volume');

        $model->save();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required'  
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = JobOrderDetail::find($request->input('id'));
        $model->delete();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

}
