<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductOperation;
use Response;
use Validator;
use Config;
use Auth;

class ProductOperationController extends Controller
{
    protected function show(Request $request)
    {
        if($request->has('id'))
        {

            $model = ProductOperation::find($request->input('id'));
        }
        else if($request->has('product_id'))
        {
            $model = ProductOperation::where('product_id', $request->input('product_id'))->orderBy('step_number', 'asc')->get();;
        }
        else
        {

            $model = ProductOperation::all();
        }
        

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'product_id'        => 'required',
            'name'              => 'required',
            'step_number'       => 'required'
            
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model                      = new ProductOperation;
        $model->product_id          = $request->input('product_id');
        $model->name                = $request->input('name');
        if($request->input('description',null) !== null) $model->description = $request->input('description');
        $model->step_number         = $request->input('step_number');
        $model->created_by          = Auth::user()->id;
        $model->save();

        $result = array('result' => true , 'message' => 'Insert Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function update(Request $request)
    {
        $validator = Validator::make
        (
            $request->all(), [ 'id' => 'required' ]
        );

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages() ), 400);
        }

        $model = ProductOperation::find($request->input('id'));
 
        if($request->input('name',null) !== null) $model->name = $request->input('name');
        if($request->input('description',null) !== null) $model->description = $request->input('description');
        if($request->input('step_number',null) !== null) $model->step_number = $request->input('step_number');

        $model->save();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required'  
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = ProductOperation::find($request->input('id'));
        $model->delete();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

}
