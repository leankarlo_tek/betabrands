<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductType;
use Response;
use Validator;
use Config;

class ProductTypeController extends Controller
{
    protected function show(Request $request)
    {
        if($request->has('id'))
        {
            $model = ProductType::find($request->input('id'));
        }
        else
        {
            $model = ProductType::all();
        }

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'                  => 'required|unique:productTypes'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model                      = new ProductType;
        $model->name                = $request->input('name');
        if($request->input('description',null) !== null) $model->description = $request->input('description');
        if($request->input('bundle_size',null) !== null) $model->bundle_size = $request->input('bundle_size');
        $model->created_by          = 1;
        $model->save();

        $result = array('result' => true , 'message' => 'Insert Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required'  

        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = ProductType::find($request->input('id'));
 
        if($request->input('name',null) !== null) $model->name = $request->input('name');
        if($request->input('description',null) !== null) $model->description = $request->input('description');
        if($request->input('bundle_size',null) !== null) $model->bundle_size = $request->input('bundle_size');

        $model->save();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

}
