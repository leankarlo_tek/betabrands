<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JobOrderStatus;
use Response;
use Validator;
use Config;

class JobOrderStatusController extends Controller
{
    protected function show(Request $request)
    {
        if($request->has('id'))
        {
            $model = JobOrderStatus::find($request->input('id'));
        }
        else
        {
            $model = JobOrderStatus::all();
        }

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'                  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model                      = new JobOrderStatus;
        $model->name                = $request->input('name');
        if($request->input('description',null) !== null) $model->description = $request->input('description');
        $model->created_by          = Auth::user()->id;
        $model->save();

        $result = array('result' => true , 'message' => 'Insert Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required'  

        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = JobOrderStatus::find($request->input('id'));
 
        if($request->input('name',null) !== null) $model->name = $request->input('name');
        if($request->input('description',null) !== null) $model->description = $request->input('description');

        $model->save();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

}
