<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use Response;
use Validator;
use Config;

class ClientController extends Controller
{
    protected function show(Request $request)
    {
        $input = $request->all();

        $sort = $request->input('sort', 'desc');
        $orderBy = $request->input('order', 'created_at');
        $model = null;

        if( $sort != 'asc' && $sort != 'desc' )
        {
             $sort = 'asc';
        }

        if($request->has('id'))
        {
            $model = Client::find($request->input('id'));
        }
        if($request->has('key'))
        {
            $model = Client::where('name', 'like', '%'.$request->input('key').'%')->get();
        }
        else
        {
            $model = Client::orderBy($orderBy, $sort)->get();
        }

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'                  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model                      = new Client;
        $model->name                = $request->input('name');
        $model->save();

        $result = array('result' => true , 'message' => 'Insert Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required'  

        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = Client::find($request->input('id'));
 
        if($request->input('name',null) !== null) $model->name = $request->input('name');

        $model->save();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

}
