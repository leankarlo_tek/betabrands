<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GovermentInformation;
use Response;
use Validator;
use Config;

class GovermentInfoController extends Controller
{
    protected function show(Request $request)
    {
        $input = $request->all();

        $sort = $request->input('sort', 'desc');
        $orderBy = $request->input('order', 'created_at');
        $model = null;

        if( $sort != 'asc' && $sort != 'desc' )
        {
             $sort = 'asc';
        }

        if($request->has('id'))
        {
            $model = GovermentInformation::find($request->input('id'));
        }
        else
        {
            $model = GovermentInformation::orderBy($orderBy, $sort)->get();
        }

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'                  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model                          = new GovermentInformation;
        $model->user_id                 = $request->input('user_id');
        
        if($request->input('TIN',null) !== null)                $model->TIN                 = $request->input('TIN');
        if($request->input('SSS',null) !== null)                $model->SSS                 = $request->input('SSS');
        if($request->input('PAGIBIG',null) !== null)            $model->PAGIBIG             = $request->input('PAGIBIG');

        $model->save();

        $result = array('result' => true , 'message' => 'Insert Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function update(Request $request)
    {
        if ( $request->has('id') )
        {
            $model = GovermentInformation::find($request->input('id'));

            if(!$model)
            {
                $result = array('result' => false , 'message' => "Can't find data");
                return Response::json( $result );
            }
        }
        else if ( $request->has('user_id') )
        {
            $model = GovermentInformation::where( 'user_id', $request->input('user_id') )->first();

            if(!$model){
                $model = new GovermentInformation;
                $model->user_id = $request->input('user_id') ;
            }
        }
        else
        {
             return response()->json(array('result' => false, 'message' => 'id or user_id should exist', 400));
        }
        
        if($request->input('TIN',null) !== null)                $model->TIN                 = $request->input('TIN');
        if($request->input('SSS',null) !== null)                $model->SSS                 = $request->input('SSS');
        if($request->input('PAGIBIG',null) !== null)            $model->PAGIBIG             = $request->input('PAGIBIG');

        $model->save();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

}
