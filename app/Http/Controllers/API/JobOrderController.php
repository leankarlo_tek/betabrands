<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JobOrder;
use App\Models\JobOrderDetail;
use App\Models\ProductOperation;
use App\Models\JobOrderDevelopment;
use Response;
use Validator;
use Config;
use Auth;
use Yajra\Datatables\Facades\Datatables;

class JobOrderController extends Controller
{
	protected function show(Request $request)
	{

		//INITIALIZATION
        $input = $request->all();

        $sort = $request->input('sort', 'desc');
        $orderBy = $request->input('order', 'created_at');
        $model = null;

        if( $sort != 'asc' && $sort != 'desc' )
        {
             $sort = 'asc';
        }

        if ($request->has('key') && $request->has('filter') )
        {
            switch ($request->input('filter') ) {
                case 'product':

                	$model = JobOrder::leftJoin('products as P', 'P.id', '=', 'jobOrders.product_id')
                        ->select('jobOrders.*')
                        ->where('P.name', 'like', '%'.$input['key'].'%')
                        ->orWhere('P.description', 'like', '%'.$input['key'].'%')
                        ->distinct('products.id')
                        ->with('Client','Product','Color','Size','Detail.Development','Status')
                        ->orderBy($orderBy, $sort)
                        ->get();

                    
                    break;

                case 'status':

                	$model = JobOrder::where('status', 'like', '%'.$input['key'].'%')
                        ->distinct('id')
                        ->with('Client','Product','Color','Size','Detail.Development','Status')
                        ->orderBy($orderBy, $sort)
                        ->get();

                    
                    break;                

                default:

                	$model = JobOrder::leftJoin('products as P', 'P.id', '=', 'jobOrders.product_id')
                        ->select('jobOrders.*')
                        ->where('P.name', 'like', '%'.$input['key'].'%')
                        ->orWhere('P.description', 'like', '%'.$input['key'].'%')
                        ->distinct('products.id')
                        ->with('Client','Product','Color','Size','Detail.Development','Status')
                        ->orderBy($orderBy, $sort)
                        ->get();
                    
                    break;
            }
        }
        else if ($request->has('key') )
        {
            $model = JobOrder::leftJoin('products as P', 'P.id', '=', 'jobOrders.product_id')
                        ->select('jobOrders.*')
            			->where('P.name', 'like', '%'.$input['key'].'%')
            			->orWhere('P.description', 'like', '%'.$input['key'].'%')
                        ->distinct('products.id')
            			->with('Client','Product','Color','Size','Detail.Development','Status')
                        ->orderBy($orderBy, $sort)
                        ->get();
        }
        else if ($request->has('id') )
        {
            
            $model = JobOrder::where('id',$request->input('id'))
            			->with('Client','Product','Detail.Development','Status')
                        ->first();
        }
        else
        {
            $model = JobOrder::with('Client','Product','Detail.Development','Status')
                        ->orderBy($orderBy, $sort)
                        ->get();
        }

        if($model == null){

            $result = array('result' => false, 'message' => 'No Product Found!' );
            return Response::json( $result );

        }

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

        // return Datatables::of($model)->make(true);

	}

	protected function create(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'client_id'            => 'required|integer',
			'volume'                => 'required|integer'
		]);

		if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		$model                      = new JobOrder;
        $model->client_id           = $request->input('client_id');
		$model->volume              = $request->input('volume');
		if($request->input('total_bundles',null)  !== null) $model->total_bundles     = $request->input('total_bundles');
		if($request->input('c_jo',null)           !== null) $model->c_jo              = $request->input('c_jo');
		$model->created_by          = 1;
		$model->status          	= 2;

		$model->save();

		$result = array('result' => true , 'message' => 'Insert Success');
		$result = array_add($result, 'data' , $model);
		return Response::json( $result );

	}

	protected function update(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'id'            => 'required'  
		]);

		if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		$model = JobOrder::find($request->input('id'));
 
		if($request->input('volume',null)         !== null) $model->volume        = $request->input('volume');
		if($request->input('total_bundles',null)  !== null) $model->total_bundles = $request->input('total_bundles');
		if($request->input('c_jo',null)           !== null) $model->c_jo          = $request->input('c_jo');
        if($request->input('status',null)           !== null) $model->status      = $request->input('status');

		$model->save();

		$result = array('result' => true , 'message' => 'Update Success');
		$result = array_add($result, 'data' , $model);
		return Response::json( $result );

	}

	protected function delete(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'id'            => 'required'  

		]);

		if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		$model = JobOrderDetail::find($request->input('id'));
		$model->delete();

		$result = array('result' => true , 'message' => 'Update Success');
		$result = array_add($result, 'data' , $model);
		return Response::json( $result );

	}

}
