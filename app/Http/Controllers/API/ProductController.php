<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Response;
use Validator;
use Config;

class ProductController extends Controller
{
    protected function show(Request $request)
    {

        //INITIALIZATION
        $input = $request->all();

        $sort = $request->input('sort', 'asc');
        $orderBy = 'products.' . $request->input('order', 'created_at');
        $model = null;

        if( $sort != 'asc' && $sort != 'desc' )
        {
             $sort = 'asc';
        }

        if ($request->has('key') && $request->has('filter') )
        {
            switch ($request->input('filter') ) {
                case 'operation':

                    $model = Product::leftJoin('productOperations as operation', 'operation.product_id', '=', 'products.id')
                            ->select('products.*')
                            ->orWhere('operation.name', 'like', '%'.$request->input('key').'%')
                            ->distinct('products.id')
                            ->with('Type','Operations')
                            ->orderBy($orderBy, $sort)
                            ->get();

                    break;

                case 'type':

                    $model = Product::leftJoin('productTypes as type', 'products.type_id', '=', 'type.id')
                            ->select('products.*')
                            ->orWhere('type.name', 'like', '%'.$request->input('key').'%')
                            ->distinct('products.id')
                            ->with('Type','Operations')
                            ->orderBy($orderBy, $sort)
                            ->get();

                    break;

                default:
                
                    $model = Product::leftJoin('productTypes as type', 'products.type_id', '=', 'type.id')
                            ->leftJoin('productOperations as operation', 'operation.product_id', '=', 'products.id')
                            ->select('products.*')
                            ->orWhere('products.id', 'like', '%'.$request->input('key').'%')
                            ->orWhere('products.name', 'like', '%'.$request->input('key').'%')
                            ->orWhere('type.name', 'like', '%'.$request->input('key').'%')
                            ->orWhere('type.description', 'like', '%'.$request->input('key').'%')
                            ->orWhere('operation.name', 'like', '%'.$request->input('key').'%')
                            ->orWhere('operation.description', 'like', '%'.$request->input('key').'%')
                            ->distinct('products.id')
                            ->with('Type','Operations')
                            ->orderBy($orderBy, $sort)
                            ->get();

                    break;
            }
        }
        else if ($request->has('key') )
        {
            $model = Product::leftJoin('productTypes as type', 'products.type_id', '=', 'type.id')
                            ->leftJoin('productOperations as operation', 'operation.product_id', '=', 'products.id')
                            ->select('products.*')
                            ->orWhere('products.id', 'like', '%'.$request->input('key').'%')
                            ->orWhere('products.name', 'like', '%'.$request->input('key').'%')
                            ->orWhere('type.name', 'like', '%'.$request->input('key').'%')
                            ->orWhere('type.description', 'like', '%'.$request->input('key').'%')
                            ->orWhere('operation.name', 'like', '%'.$request->input('key').'%')
                            ->orWhere('operation.description', 'like', '%'.$request->input('key').'%')
                            ->distinct('products.id')
                            ->with('Type','Operations')
                            ->orderBy($orderBy, $sort)
                            ->get();
        }
        else if ($request->has('id') )
        {
            $model = Product::where('id',$request->input('id'))
                            ->with('Type','Operations')
                            ->first();
        }
        else
        {
            $model = Product::with('Type')
                            ->orderBy($orderBy, $sort)
                            ->get();
                            // ->paginate(1);
        }

        if($model == null){

            $result = array('result' => false, 'message' => 'No Product Found!' );
            return Response::json( $result );

        }

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'                      => 'required',
            'type_id'                   => 'required|integer',
            'created_by'                => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model                      = new Product;
        $model->name                = $request->input('name');
        if($request->input('code',null) !== null) $model->code = $request->input('code');
        if($request->input('description',null) !== null) $model->description = $request->input('description');
        $model->type_id             = $request->input('type');
        $model->created_by          = Auth::user()->id;
        $model->save();

        $result = array('result' => true , 'message' => 'Insert Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

    protected function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'            => 'required',
            'type_id'       => 'integer'        

        ]);

        if ($validator->fails()) {
            return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
        }

        $model = Product::find($request->input('id'));
 
        if($request->input('name',null) !== null) $model->name = $request->input('name');
        if($request->input('description',null) !== null) $model->description = $request->input('description');
        if($request->input('type_id',null) !== null) $model->type_id = $request->input('type_id');

        $model->save();

        $result = array('result' => true , 'message' => 'Update Success');
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

}
