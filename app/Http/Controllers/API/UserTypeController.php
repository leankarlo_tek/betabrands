<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserType;
use Response;
use Validator;
use Config;

class UserTypeController extends Controller
{
    protected function show(Request $request)
    {
        if($request->has('id'))
        {
            $model = UserType::find($request->input('id'));
        }
        else
        {
            $model = UserType::all();
        }

        $result = array('result' => true);
        $result = array_add($result, 'data' , $model);
        return Response::json( $result );

    }

}
