<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JobOrderBundleOperations;
use Response;
use Validator;
use Config;
use Auth;

class JobOrderBundleOperationsController extends Controller
{
	protected function show(Request $request)
	{
		$model = null;
		if($request->has('id'))
		{
			$model = JobOrderBundleOperations::find($request->input('id'));
		}
		else if($request->has('bundle_id'))
		{
			$model = JobOrderBundleOperations::where('bundle_id',$request->input('bundle_id'))->with('Operation','Status')->get();
		}

		if($model == null)
		{
			$result = array('result' => false,'message' => 'Bundle cannot be found!');
			$result = array_add($result, 'data' , $model);
			return Response::json( $result );
		}

		$result = array('result' => true);
		$result = array_add($result, 'data' , $model);
		return Response::json( $result );

	}

	protected function create(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'bundle_id'         => 'required|integer'
		]);

		if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		$model                          			= new JobOrderBundleOperations;
		$model->bundle_id                    		= $request->input('bundle_id');
		if($request->input('operation_id',null)		!== null) $model->operation_id		= $request->input('operation_id');
		if($request->input('assigned_to',null)		!== null) $model->assigned_to       = $request->input('assigned_to');
		if($request->input('started_on',null)       !== null) $model->started_on        = $request->input('started_on');
		if($request->input('ended_on',null)         !== null) $model->ended_on          = $request->input('ended_on');
		if($request->input('status',null)           !== null) $model->status            = $request->input('status');

		$model->save();

		$result = array('result' => true , 'message' => 'Insert Success');
		$result = array_add($result, 'data' , $model);
		return Response::json( $result );

	}

	protected function update(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'id'            => 'required'  

		]);

		if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		$model = JobOrderBundleOperations::find($request->input('id'));
 
 		if($request->input('bundle_id',null)		!== null) $model->bundle_id		= $request->input('bundle_id');
		if($request->input('operation_id',null)		!== null) $model->operation_id		= $request->input('operation_id');
		if($request->input('assigned_to',null)		!== null) $model->assigned_to       = $request->input('assigned_to');
		if($request->input('started_on',null)       !== null) $model->started_on        = $request->input('started_on');
		if($request->input('ended_on',null)         !== null) $model->ended_on          = $request->input('ended_on');
		if($request->input('status',null)           !== null) $model->status            = $request->input('status');
		
		$model->save();

		$result = array('result' => true , 'message' => 'Update Success');
		$result = array_add($result, 'data' , $model);
		return Response::json( $result );

	}

	protected function delete(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'id'            => 'required'  
		]);

		if ($validator->fails()) {
			return response()->json(array('result' => false, 'message' => $validator->messages()), 400);
		}

		$model = JobOrderBundleOperations::find($request->input('id'));
		$model->delete();

		$result = array('result' => true , 'message' => 'Delete Success');
		$result = array_add($result, 'data' , $model);
		return Response::json( $result );

	}

}
