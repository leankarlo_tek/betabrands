<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Product extends Model
{

	protected $table = 'products';

	public function Operations()
    {
        return $this->hasMany('App\Models\ProductOperation', 'product_id', 'id');
    }

    public function Type()
    {
        return $this->hasOne('App\Models\ProductType', 'id', 'type_id');
    }

}
