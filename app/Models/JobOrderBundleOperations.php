<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobOrderBundleOperations extends Model
{
    // protected $table = 'job_order_bundles';

    public function Detail()
    {
        return $this->belongsTo('App\Models\JobOrderBundles', 'bundle_id', 'id');
    }

    public function Status()
    {
        return $this->hasOne('App\Models\JobOrderStatus', 'id', 'status');
    }

    public function Operation()
    {
        return $this->hasOne('App\Models\ProductOperation', 'id', 'operation_id');
    }
}
