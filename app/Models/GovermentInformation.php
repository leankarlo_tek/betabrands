<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GovermentInformation extends Model
{
    protected $table = 'userGovermentInfo';

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

}
