<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Type()
    {
        return $this->hasOne('App\Models\UserType', 'id', 'type');
    }

    public function PersonalInformation()
    {
        return $this->hasOne('App\Models\PersonalInformation', 'user_id', 'id');
    }

    public function GovermentInformation()
    {
        return $this->hasOne('App\Models\GovermentInformation', 'user_id', 'id');
    }
}
