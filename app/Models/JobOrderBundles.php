<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class JobOrderBundles extends Model
{
    protected $table = 'job_order_bundles';

    public function Detail()
    {
        return $this->belongsTo('App\Models\JobOrderDetail', 'job_detail_id', 'id');
    }

    public function Operation()
    {
    	return $this->hasMany('App\Models\JobOrderBundleOperations', 'id', 'bundle_id');
    }

    public function Product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function Color()
    {
        return $this->hasOne('App\Models\ProductColor', 'id', 'color_id');
    }

    public function Size()
    {
        return $this->hasOne('App\Models\ProductSize', 'id', 'size_id');
    }

    public function Status()
    {
        return $this->hasOne('App\Models\JobOrderStatus', 'id', 'status');
    }


}
