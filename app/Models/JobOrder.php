<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class JobOrder extends Model
{

	protected $table = 'jobOrders';

    public function Client()
    {
        return $this->hasOne('App\Models\Client', 'id', 'client_id');
    }

    public function Detail()
    {
        return $this->hasMany('App\Models\JobOrderDetail', 'id', 'jo_id');
    }

	public function Product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function Color()
    {
        return $this->hasOne('App\Models\ProductColor', 'id', 'color_id');
    }

    public function Size()
    {
        return $this->hasOne('App\Models\ProductSize', 'id', 'size_id');
    }

    public function Status()
    {
        return $this->hasOne('App\Models\JobOrderStatus', 'id', 'status');
    }


}
