<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalInformation extends Model
{
    protected $table = 'userPersonalInfo';

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

}
