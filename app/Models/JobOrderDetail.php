<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class JobOrderDetail extends Model
{

	protected $table = 'jobOrderDetails';

    public function Bundle()
    {
        return $this->hasMany('App\Models\JobOrderBundles', 'id', 'job_detail_id');
    }

    public function Development()
    {
        return $this->hasMany('App\Models\JobOrderDevelopment', 'id', 'jo_detail_id');
    }

    public function Product()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }

    public function Status()
    {
        return $this->hasOne('App\Models\JobOrderStatus', 'id', 'status');
    }

}