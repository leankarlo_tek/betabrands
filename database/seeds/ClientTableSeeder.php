<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            
            [ 	
            	'name' 			=> 'Teknolohiya'
            ],

            [ 	
            	'name' 			=> 'Nike'
            ],

            [ 	
            	'name' 			=> 'Bench'
            ],

            [ 	
            	'name' 			=> 'Penshop'
            ],

            [   
                'name'          => 'H&M'
            ],

            [   
                'name'          => 'Uniqlo'
            ],


        ]);
    }
}
