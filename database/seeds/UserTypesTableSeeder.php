<?php

use Illuminate\Database\Seeder;

class UserTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('userTypes')->insert([
            
            [ 	
            	'id'			=> 1,
            	'name' 			=> 'Administator',
            	'description' 	=> 'Application Admin'
            ],

            [ 	
            	'id'			=> 2,
            	'name' 			=> 'Manager',
            	'description' 	=> 'Application Manager'
            ],

            [ 	
            	'id'			=> 3,
            	'name' 			=> 'Assistant Manager',
            	'description' 	=> 'Application Manager'
            ],

            [ 	
            	'id'			=> 4,
            	'name' 			=> 'Application Personel',
            	'description' 	=> 'Application User'
            ],

            [ 	
            	'id'			=> 5,
            	'name' 			=> 'Factory Worker',
            	'description' 	=> 'No access to this application'
            ],

        ]);
    }
}
