<?php

use Illuminate\Database\Seeder;

class JobOrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobOrderStatus')->insert([
            
            [ 	
                'id'            => 1,
            	'name' 			=> 'Draft',
            	'description' 	=> 'Job order is still on draft'
            ],

            [ 	
                'id'            => 2,
            	'name' 			=> 'New Request',
            	'description' 	=> 'Job order is now sent for manufacturing'
            ],

            [ 	
                'id'            => 3,
            	'name' 			=> 'Confirmed',
            	'description' 	=> 'Job order is now on queue'
            ],

            [ 	
                'id'            => 4,
            	'name' 			=> 'On Progress',
            	'description' 	=> 'Job Order is now on progress'
            ],

            [ 	
                'id'            => 5,
            	'name' 			=> 'Quality Assurance',
            	'description' 	=> 'Job order is now on checking'
            ],

            [
                'id'            => 6,
            	'name' 			=> 'Ready for delivery',
            	'description' 	=> 'Job order is now packed and ready for delivery'
            ],

            [ 	
                'id'            => 7,
            	'name' 			=> 'Returned due to defect',
            	'description' 	=> 'Job order is returned due to defect'
            ],

            [ 	
                'id'            => 8,
            	'name' 			=> 'Re opened',
            	'description' 	=> 'Job order is now fixing the issue'
            ],

        ]);
    }
}
