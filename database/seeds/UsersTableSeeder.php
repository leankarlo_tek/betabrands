<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            
            [ 	
            	'username' 			=> 	'LeanKarlo',
            	'email' 			=> 	'lean@teknolohiya.ph',
            	'position'			=>	'Admin',
            	'firstname'			=>	'Lean Karlo',
            	'middlename'		=>	'Urrutia',
            	'lastname'			=>	'Corpuz',
            	'type'			=>	1,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	

            	'username' 			=> 	'Malcolm',
            	'email' 			=> 	'malcolm@elite.ph',
            	'position'			=>	'Admin',
            	'firstname'			=>	'Malcolm',
                  'middlename'            =>    '',
            	'lastname'			=>	'Ching',
            	'type'			=>	1,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	
            
            	'username' 			=> 	'usermanager',
            	'email' 			=> 	'usermanager@elite.ph',
            	'position'			=>	'Manager',
            	'firstname'			=>	'Mike',
                  'middlename'            =>    '',
            	'lastname'			=>	'Man',
            	'type'			=>	2,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	
            
            	'username' 			=> 	'userassistant',
            	'email' 			=> 	'userassistant@elite.ph',
            	'position'			=>	'Assistant Manager',
            	'firstname'			=>	'Jake',
                  'middlename'            =>    '',
            	'lastname'			=>	'Go',
            	'type'			=>	3,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	
            
            	'username' 			=> 	'userpersonal',
            	'email' 			=> 	'userpersonel@elite.ph',
            	'position'			=>	'Application Personel',
            	'firstname'			=>	'Jany',
                  'middlename'            =>    '',
            	'lastname'			=>	'Bueno',
            	'type'			=>	4,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	
            
            	'username' 			=> 	'factory1',
            	'email' 			=> 	'factory1@elite.ph',
            	'position'			=>	'Worker',
                  'middlename'            =>    '',
            	'firstname'			=>	'Fiona',
            	'lastname'			=>	'Salanguit',
            	'type'			=>	5,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	
            
            	'username' 			=> 	'factory2',
            	'email' 			=> 	'factory2@elite.ph',
            	'position'			=>	'Worker',
            	'firstname'			=>	'Erick',
                  'middlename'            =>    '',
            	'lastname'			=>	'Tan',
            	'type'			=>	5,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	
            
            	'username' 			=> 	'factory3',
            	'email' 			=> 	'factory3@elite.ph',
            	'position'			=>	'Worker',
            	'firstname'			=>	'Enrique',
                  'middlename'            =>    '',
            	'lastname'			=>	'Ureta',
            	'type'			=>	5,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	
            
            	'username' 			=> 	'factory4',
            	'email' 			=> 	'factory4@elite.ph',
            	'position'			=>	'Worker',
            	'firstname'			=>	'Joseph',
                  'middlename'            =>    '',
            	'lastname'			=>	'Salanguit',
            	'type'			=>	5,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	
            
            	'username' 			=> 	'factory5',
            	'email' 			=> 	'factory5@elite.ph',
            	'position'			=>	'Worker',
            	'firstname'			=>	'John',
                  'middlename'            =>    '',
            	'lastname'			=>	'Joe',
            	'type'			=>	5,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	
            
            	'username' 			=> 	'factory6',
            	'email' 			=> 	'factory6@elite.ph',
            	'position'			=>	'Worker',
            	'firstname'			=>	'Willy',
                  'middlename'            =>    '',
            	'lastname'			=>	'Bill',
            	'type'			=>	5,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	
            
            	'username' 			=> 	'factory7',
            	'email' 			=> 	'factory7@elite.ph',
            	'position'			=>	'Worker',
            	'firstname'			=>	'Willy',
                  'middlename'            =>    '',
            	'lastname'			=>	'Soreta',
            	'type'			=>	5,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	
            
            	'username' 			=> 	'factory8',
            	'email' 			=> 	'factory8@elite.ph',
            	'position'			=>	'Worker',
            	'firstname'			=>	'Mark',
                  'middlename'            =>    '',
            	'lastname'			=>	'Tan',
            	'type'			=>	5,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

            [ 	
            
            	'username' 			=> 	'factory9',
            	'email' 			=> 	'factory9@elite.ph',
            	'position'			=>	'Worker',
            	'firstname'			=>	'Ryan',
                  'middlename'            =>    '',
            	'lastname'			=>	'Bonnel',
            	'type'			=>	5,
            	'password'			=>	bcrypt('antimoda3412'),

            ],

        ]);
    }
}
