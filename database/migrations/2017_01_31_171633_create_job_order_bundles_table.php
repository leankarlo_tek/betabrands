<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobOrderBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('job_order_bundles');
        Schema::create('job_order_bundles', function (Blueprint $table) {

            $table->increments('id');
            
            $table->integer('jo_detail_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();
            $table->integer('color_id')->unsigned()->index();
            $table->integer('size_id')->unsigned()->index();
            $table->integer('bundle_quantity')->nullable();

            $table->dateTime('created_at')->default(date("Y-m-d H:i:s"));
            $table->timestamp('updated_at')->default(date("Y-m-d H:i:s"));
        });


        Schema::table('job_order_bundles', function (Blueprint $table) {
            $table->foreign('jo_detail_id')->references('id')->on('jobOrderDetails')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('color_id')->references('id')->on('productColors');
            $table->foreign('size_id')->references('id')->on('productSizes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_order_bundles');
    }
}
