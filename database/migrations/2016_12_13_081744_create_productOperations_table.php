<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOperationsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('productOperations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('name')->unique();
            $table->string('description')->nullable();
            $table->integer('step_number');
            $table->integer('created_by')->default(0);
            
            $table->dateTime('created_at')->default(date("Y-m-d H:i:s"));
            $table->timestamp('updated_at')->default(date("Y-m-d H:i:s"));
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::drop('productOperations');
    }
}
