<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyJobOrderAndJobOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        // drop column from JobOrder since those columns are not for JobORder instead is needed on
        // the JobOrder Batch which is the JobOrderDetails

        Schema::table('jobOrders', function (Blueprint $table) 
        {
            $table->dropColumn(['color_id','size_id','size_volume','size_bundle','quantity_per_batch']);
        
        });


        Schema::table('jobOrderDetails', function (Blueprint $table) 
        {
            $table->dropColumn('bundle_quantity');

        });

        Schema::table('jobOrderDetails', function (Blueprint $table) 
        {
            $table->integer('size_volume');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobOrders', function (Blueprint $table) {
            $table->integer('color_id');
            $table->integer('size_id');
            $table->integer('size_volume');
            $table->integer('size_bundle');
            $table->integer('quantity_per_batch');
        });

        Schema::table('jobOrderDetails', function (Blueprint $table) {
            $table->string('bundle_quantity')->nullable();
        });

        Schema::table('jobOrderDetails', function(Blueprint $table)
        {
            $table->dropColumn(['size_volume']);

        });
    }
}