<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPersonalInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userPersonalInfo', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('user_id'); 

            $table->date('birthday')->nullable();
            $table->integer('age')->mullable();

            $table->string('religion')->nullable();
            $table->string('nationality')->nullable();

            $table->string('birth_place')->nullable();

            $table->string('father_lastname')->nullable();
            $table->string('father_firstname')->nullable();
            $table->string('father_middlename')->nullable();

            $table->string('mother_lastname')->nullable();
            $table->string('mother_middlename')->nullable();
            $table->string('mother_firstname')->nullable();

            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('address3')->nullable();
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('country')->nullable();
            
            $table->dateTime('created_at')->default(date("Y-m-d H:i:s"));
            $table->timestamp('updated_at')->default(date("Y-m-d H:i:s"));

        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::drop('userPersonalInfo');
    }
}
