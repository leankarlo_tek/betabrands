<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobOrderDetails', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jo_id');
            $table->integer('batch');
            $table->string('batch_code')->nullable();
            $table->string('bundle_quantity')->nullable();
            $table->integer('created_by')->default(0);
            
            $table->dateTime('created_at')->default(date("Y-m-d H:i:s"));
            $table->timestamp('updated_at')->default(date("Y-m-d H:i:s"));
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::drop('jobOrderDetails');
    }
}
