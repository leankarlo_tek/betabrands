<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobOrderBunleOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_order_bundle_operations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('bundle_id')->unsigned()->index();
            $table->integer('operation_id')->unsigned()->index();
            $table->datetime('started_on')->nullable();
            $table->datetime('ended_on')->nullable();
            $table->integer('assigned_to')->unsigned()->index();

            $table->dateTime('created_at')->default(date("Y-m-d H:i:s"));
            $table->timestamp('updated_at')->default(date("Y-m-d H:i:s"));
        });

        Schema::table('job_order_bundle_operations', function (Blueprint $table) {
            $table->foreign('bundle_id')->references('id')->on('job_order_bundles');
            $table->foreign('operation_id')->references('id')->on('productOperations');
            $table->foreign('assigned_to')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_order_bundle_operations');
    }
}
