<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateJobOrderFielsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobOrderDetails', function($table)
        {
            $table->dropColumn(['size_volume']);
        });

        Schema::table('job_order_bundles', function($table)
        {
            $table->dropColumn(['product_id']);
        });

        Schema::table('jobOrderDetails', function (Blueprint $table) {
            $table->integer('batch_volume')->nullable();
            $table->integer('product_id')->unsigned()->index();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobOrderDetails', function($table)
        {
            $table->integer(['size_volume'])->nullable();
        });

        Schema::table('job_order_bundles', function($table)
        {
            $table->integer('product_id')->unsigned()->index();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });

        Schema::table('jobOrderDetails', function (Blueprint $table) {
            $table->dropColumn(['product_id','batch_volume']);
        });
    }
}
