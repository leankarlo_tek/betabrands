<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldJobOrderBundleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_order_bundles', function (Blueprint $table) {

            $table->datetime('started_on')->nullable();
            $table->datetime('ended_on')->nullable();

            $table->integer('status')->unsigned()->index();
            $table->foreign('status')->references('id')->on('jobOrderStatus')->onDelete('cascade');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_order_bundles', function($table)
        {
            $table->dropColumn(['started_on','ended_on']);

        });
    }
}
