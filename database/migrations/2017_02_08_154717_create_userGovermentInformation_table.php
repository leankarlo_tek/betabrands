<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGovermentInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userGovermentInfo', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->integer('user_id'); 

            $table->string('TIN')->nullable();
            $table->string('SSS')->nullable();
            $table->string('PAGIBIG')->nullable();
            
            $table->dateTime('created_at')->default(date("Y-m-d H:i:s"));
            $table->timestamp('updated_at')->default(date("Y-m-d H:i:s"));
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::drop('userGovermentInfo');
    }
}
