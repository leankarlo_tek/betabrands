<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobOrderDevelopmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobOrderDevelopment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jo_detail_id'); 
            $table->string('barcode'); /* 4 digit JO NUMBER + batch number + SIZE + jo_detail_id 3 digits + operation step number 2 (digits) */
            $table->integer('product_operation_id');
            $table->datetime('start')->nullable();
            $table->datetime('end')->nullable();
            $table->integer('user_id')->nullable();
            $table->tinyInteger('isFinished')->default(0);
            $table->integer('created_by')->default(0);
            
            $table->dateTime('created_at')->default(date("Y-m-d H:i:s"));
            $table->timestamp('updated_at')->default(date("Y-m-d H:i:s"));
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::drop('jobOrderDevelopment');
    }
}
