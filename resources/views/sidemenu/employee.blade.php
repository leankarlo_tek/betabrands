<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            
            <li class="heading">
                <h3 class="uppercase">Employee</h3>
            </li>
            
            <li class="nav-item  ">
                <a href="{{ URL::to('../admin/employee') }}" class="nav-link nav-toggle">
                    <i class="fa fa-list-ul"></i>
                    <span class="title">List</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="{{ URL::to('../admin/employee/add') }}" class="nav-link nav-toggle">
                    <i class="icon-plus"></i>
                    <span class="title">Add</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="{{ URL::to('../admin/employee/upload') }}" class="nav-link nav-toggle">
                    <i class="icon-cloud-upload"></i>
                    <span class="title">Upload Multiple Employee</span>
                </a>
            </li>
        </ul>
    </div>
</div>

<!-- /.modal -->

<!-- /.modal -->