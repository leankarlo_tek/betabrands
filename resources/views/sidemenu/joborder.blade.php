<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            
            <li class="heading">
                <h3 class="uppercase">Job Order</h3>
            </li>
            
            <li class="nav-item  ">
                <a href="{{ URL::to('../admin/joborder') }}" class="nav-link nav-toggle">
                    <i class="fa fa-list-ul"></i>
                    <span class="title">List</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="{{ URL::to('../admin/joborder/add') }}" class="nav-link nav-toggle">
                    <i class="icon-plus"></i>
                    <span class="title">New</span>
                </a>
            </li>
             <li class="heading">
                <h3 class="uppercase">Product</h3>
            </li>
            <li class="nav-item  ">
                <a href="#addColor" data-toggle="modal" class="nav-link nav-toggle">
                    <i class="fa fa-paint-brush"></i>
                    <span class="title">Add Color</span>
                </a>
            </li>
            <li class="nav-item  ">
                <a href="#addSize" data-toggle="modal" class="nav-link nav-toggle">
                    <i class="fa fa-circle-o"></i>
                    <span class="title">Add Size</span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>

<!-- /.modal -->

<div class="modal fade" id="addColor" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Color Form</h4>
            </div>
            {!! Form::open(['action' => 'API\ProductColorController@create' , 'id' => 'form_color', 'method'=>'post' ,'class'=>'form-horizontal']) !!}
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Color
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-4">
                                <input type="text" name="name" id="name" data-required="1" class="form-control" /> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal" >Close</button>
                    <button type="submit" class="btn green" href="#ajax" >Save changes</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade" id="addSize" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Size Form</h4>
            </div>
            {!! Form::open(['action' => 'API\ProductSizeController@create' , 'id' => 'form_size', 'method'=>'post' ,'class'=>'form-horizontal']) !!}
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Size
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-4">
                                <input type="text" name="name" id="name" data-required="1" class="form-control" /> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal" >Close</button>
                    <button type="submit" class="btn green" href="#ajax" >Save changes</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- /.modal -->