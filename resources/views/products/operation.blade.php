@extends('layout.layout')

@section('title')

@stop

@section('head')
<link href="{{ asset('packages/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/select2/css/select2-bootstrap.min.css" rel="stylesheet') }}" type="text/css" />
<link href="{{ asset('packages/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('sidebar')
	@include('sidemenu.product')
@stop

@section('content')

<div class="page-head">
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-dark">
						<i class="icon-settings font-dark"></i>
						<span class="caption-subject bold uppercase"> Product Operations</span>
					</div>
					<div class="alert alert-success display-hide">
						<button class="close" data-close="alert"></button> 
						Product was successfully added
					</div>
				</div>
				<div class="portlet-body">
					<div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="#add" data-toggle="modal" id="sample_editable_1_new" class="btn sbold green"> 
                                    	Add New
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
					<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
						<thead>
							<tr>
								<th> Step Number </th>
								<th> Name </th>
								<th> Description </th>
								<th> Actions </th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

</div>

<!-- /.modal -->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add Item Operation</h4>
			</div>
			{!! Form::open(['action' => 'API\ProductOperationController@create' , 'id' => 'form_sample_1', 'method'=>'post' ,'class'=>'form-horizontal']) !!}
				<input type="hidden" name="product_id" id="product_id">
				<div class="modal-body">
					<div class="form-body">
						<div class="alert alert-danger display-hide">
							<button class="close" data-close="alert"></button> 
							You have some form errors. Please check below. 
						</div>
						<div class="alert alert-success display-hide">
							<button class="close" data-close="alert"></button> 
							Product was successfully added
						</div>
	
						<div class="form-group">
							<label class="control-label col-md-3">Name
								<span class="required"> * </span>
							</label>
							<div class="col-md-4">
								<input type="text" name="name" id="name" data-required="1" class="form-control" /> 
							</div>
						</div>
	
	
						<div class="form-group">
							<label class="control-label col-md-3">Description
								<span class="required"> * </span>
							</label>
							<div class="col-md-4">
								<textarea class="form-control" name="description" id="description" style="height:200px;"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Step Number
								<span class="required"> * </span>
							</label>
							<div class="col-md-4">
								<input type="text" name="step_number" id="step_number" data-required="1" class="form-control" /> 
							</div>
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" >Close</button>
					<button type="submit" class="btn green" >Save changes</button>
				</div>
			
			{!! Form::close() !!}
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit Operation</h4>
			</div>
			{!! Form::open(['action' => 'API\ProductOperationController@update' , 'id' => 'form_sample_2', 'method'=>'PUT' ,'class'=>'form-horizontal']) !!}
				<input type="hidden" name="id" id="id">
				<div class="modal-body">
					<div class="form-body">
						<div class="alert alert-danger display-hide">
							<button class="close" data-close="alert"></button> 
							You have some form errors. Please check below. 
						</div>
						<div class="alert alert-success display-hide">
							<button class="close" data-close="alert"></button> 
							Product was successfully added
						</div>
	
						<div class="form-group">
							<label class="control-label col-md-3">Name
								<span class="required"> * </span>
							</label>
							<div class="col-md-4">
								<input type="text" name="name" id="name" data-required="1" class="form-control" /> 
							</div>
						</div>
	
	
						<div class="form-group">
							<label class="control-label col-md-3">Description
								<span class="required"> * </span>
							</label>
							<div class="col-md-4">
								<textarea class="form-control" name="description" id="description" style="height:200px;"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Step Number
								<span class="required"> * </span>
							</label>
							<div class="col-md-4">
								<input type="text" name="step_number" id="step_number" data-required="1" class="form-control" /> 
							</div>
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" >Close</button>
					<button type="submit" class="btn green" >Save changes</button>
				</div>
			
			{!! Form::close() !!}
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection


@section('buttom_scripts')

<!-- BEGIN PAGE SCRIPTS -->
<script src="{{ asset('js/pages/products.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{ asset('js/pages/product_operation.js') }}" type="text/javascript"></script>
<!-- END PAGE SCRIPTS -->

@stop