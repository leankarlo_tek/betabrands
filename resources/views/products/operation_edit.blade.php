@extends('layout.layout')

@section('title')

@stop

@section('head')

<link href="{{ asset('packages/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/select2/css/select2-bootstrap.min.css" rel="stylesheet') }}" type="text/css" />
<link href="{{ asset('packages/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('sidebar')
	@include('sidemenu.product')
@stop

@section('content')

<div class="page-head">
	<div class="page-title">
		<h1>Product Management</h1>
	</div>

	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-pencil-square-o"></i>
						<span class="caption-subject sbold uppercase">Product Edit
						</span>
					</div>
				</div>
				<div class="portlet-body">
					<!-- BEGIN FORM-->
					{{-- <form action="#" id="form_sample_1" class="form-horizontal"> --}}
					{!! Form::open(['action' => 'API\ProductController@update' , 'id' => 'form_sample_1', 'method'=>'PUT' ,'class'=>'form-horizontal']) !!}
						<input type="hidden" name="id" id="id" value="" /> 
						<div class="form-body">
							<div class="alert alert-danger display-hide">
								<button class="close" data-close="alert"></button> 
								You have some form errors. Please check below. 
							</div>
							<div class="alert alert-success display-hide">
								<button class="close" data-close="alert"></button> 
								Product was successfully updated.
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Name
									<span class="required"> * </span>
								</label>
								<div class="col-md-4">
									<input type="text" name="name" id="name" data-required="1" class="form-control" /> 
								</div>
							</div>


							<div class="form-group">
								<label class="control-label col-md-3">Description
									<span class="required"> * </span>
								</label>
								<div class="col-md-4">
									<textarea class="form-control" name="description" id="description" style="height:200px;"></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">
									Product Type
									<span class="required"> * </span>
								</label>
								<div class="col-md-4">
									<select class="form-control" name="type" id="type">
										<option value="">Select...</option>
										<option value="1">Bra</option>
										<option value="2">V-Neck Shirt</option>
										<option value="3">Panty</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                    <button type="button" class="btn grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
					{{-- </form> --}}
					<!-- END FORM-->
				</div>
			</div>
				<!-- END VALIDATION STATES-->
		</div>
	</div>

	</div>

	@endsection


	@section('buttom_scripts')

	<!-- BEGIN PAGE SCRIPTS -->
	<script src="{{ asset('js/pages/products.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/pages/product_edit.js') }}" type="text/javascript"></script>
	<!-- END PAGE SCRIPTS -->

	@stop