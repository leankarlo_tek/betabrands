@extends('layout.layout')

@section('title')

@stop

@section('head')

<link href="{{ asset('packages/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('sidebar')
	@include('sidemenu.client')
@stop

@section('content')

<div class="page-head">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-dark">
						<i class="icon-settings font-dark"></i>
						<span class="caption-subject bold uppercase"> Client Management</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
					<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
						<thead>
							<tr>
								<th> ID </th>
								<th> Name </th>
								<th> Created_at </th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

</div>

@endsection


@section('buttom_scripts')

<!-- BEGIN PAGE SCRIPTS -->
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{ asset('packages/select2/js/select2.full.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('packages/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/client_list.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/client.js') }}" type="text/javascript"></script>
<!-- END PAGE SCRIPTS -->

@stop