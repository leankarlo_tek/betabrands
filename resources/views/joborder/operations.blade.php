@extends('layout.layout')

@section('title')

@stop

@section('head')

<link href="{{ asset('packages/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/select2/css/select2-bootstrap.min.css" rel="stylesheet') }}" type="text/css" />
<link href="{{ asset('packages/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('sidebar')
	@include('sidemenu.joborder')
@stop

@section('content')

<div class="page-head">
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <div class="btn-group pull-right">
                                <button class="btn green  btn-outline " onclick="PrintElem()">Print Ticket
                                </button>
                            </div>
                        </div>
                    </div>
				</div>
				<div class="portlet-body" id="mydiv">
					<div class="row">
						<div class="container">
							<div class="caption font-dark">
								<span class="caption-subject bold uppercase"> Production Bundle Ticket No. </span>
								<span class="bold uppercase" id="ticket"></span> - 
								<span class="product bold uppercase" id="product"></span> - 
								<span class="bold uppercase"> Size : </span>
								<span class="bold uppercase" id="size_top"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="container">
							<div class="col-md-4">
								<label>PO No : </label><label id=c_jo> </label><br>
								<label>PO Volume : </label><label id="volume"> </label><br>
								{{-- <label>PO Bundles : </label><label id=totalBundle> </label> --}}

							</div>
							<div class="col-md-4">
								<label>Color : </label><label id=color> </label><br>
								<label>Size Volume : </label><label id=batch_volume> </label><br>
								<label>Size Bundle : </label><label id=size> </label>
							</div>
							<div class="col-md-4">
								<label>Batch : </label><label id=batch> </label><br>
								<label>Batch Code : </label><label id=batch_code> </label><br>
								<label>Bundle Quantity : </label><label id=bundle_quantity> </label>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="container">
							<div class="col-md-6">
								<span class="caption-subject bold uppercase">Operator Name / Output Date</span>
							</div>
							<div class="col-md-6">
								<span class="caption-subject bold uppercase">Barcode :<label id="c_jo2"></label><label id="size2"></label><label id="bundle_number"></label> + OPRN NO </span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="container">
							<div id="oprn_cont">
								
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="container" >
							<div id="oprn_code_cont">
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

</div>


<!-- /.modal -->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Bundle Form</h4>
			</div>
			{!! Form::open(['action' => 'API\JobOrderBundleController@createWithOperations' , 'id' => 'form_sample_1', 'method'=>'post' ,'class'=>'form-horizontal']) !!}
				<input type="hidden" name="jo_detail_id" id="jo_detail_id">
				<div class="modal-body">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3">Product
								<span class="required"> * </span>
							</label>
							<div class="col-md-4">
								<input type="hidden" name="product_id" id="product_id" /> 
								<input type="text" name="product" id="product" data-required="1" class="form-control" /> 
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-md-3">Color
								<span class="required"> * </span>
							</label>
							<div class="col-md-4">
								<input type="hidden" name="color_id" id="color_id" /> 
								<input type="text" name="color" id="color" data-required="1" class="form-control" /> 
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Size
								<span class="required"> * </span>
							</label>
							<div class="col-md-4">
								<input type="hidden" name="size_id" id="size_id" /> 
								<input type="text" name="size" id="size" data-required="1" class="form-control" /> 
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Bundle Quantity
								<span class="required"> * </span>
							</label>
							<div class="col-md-4">
								<input type="text" name="bundle_quantity" id="bundle_quantity" data-required="1" class="form-control" /> 
							</div>
						</div>

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" >Close</button>
					<button type="submit" class="btn green" >Save changes</button>
				</div>
			
			{!! Form::close() !!}
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection


@section('buttom_scripts')
	<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="{{ asset('packages/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/scripts/printThis.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/pages/joborder.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/pages/bundle_operations.js') }}" type="text/javascript"></script>
	<!-- END PAGE SCRIPTS -->

@stop