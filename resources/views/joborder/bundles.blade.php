@extends('layout.layout')

@section('title')

@stop

@section('head')
<link href="{{ asset('packages/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('sidebar')
	@include('sidemenu.joborder')
@stop

@section('content')

<div class="page-head">
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-dark">
						<i class="icon-settings font-dark"></i>
						<span class="caption-subject bold uppercase"> Batch Bundles</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="container">
							<div class="col-md-4">
								<label>Client Name : </label><label id=clientName> </label><br>
								<label>Total Bundle : </label><label id=totalBundle> </label>
							</div>
							<div class="col-md-5">
								<label>Status : </label><label id=status> </label><br>
								<label>Volume : </label><label id=volume> </label>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="container">
							<div class="col-md-4">
								<label>Batch : </label><label id=batch> </label><br>
								<label>Product : </label><label id=product> </label>
							</div>
							<div class="col-md-5">
								<label>Status : </label><label id=status_batch> </label><br>
								<label>Volume Batch : </label><label id=batch_volume> </label>
							</div>
						</div>
					</div>
					
					<br>
					<div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="#add" data-toggle="modal" id="sample_editable_1_new" class="btn sbold green"> 
                                    	Add New
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
					<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
						<thead>
							<tr>
								<th> Size </th>
								<th> Color </th>
								<th> Bundle Qty </th>
								<th> Status </th>
								<th> Action </th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

</div>


<!-- /.modal -->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Bundle Form</h4>
			</div>
			{!! Form::open(['action' => 'API\JobOrderBundleController@createWithOperations' , 'id' => 'form_sample_1', 'method'=>'post' ,'class'=>'form-horizontal']) !!}
				<input type="hidden" name="jo_detail_id" id="jo_detail_id">
				<div class="modal-body">
					<div class="form-body">
						
						<div class="form-group">
							<label class="control-label col-md-3">
								Color
								<span class="required"> * </span>
							</label>
							<div class="col-md-8">
								<select class="form-control select2" id="color_id" name="color_id">
								</select>
                    	    </div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">
								Size
								<span class="required"> * </span>
							</label>
							<div class="col-md-8">
								<select class="form-control select2" id="size_id" name="size_id">
								</select>
                    	    </div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-3">Bundle Quantity
								<span class="required"> * </span>
							</label>
							<div class="col-md-4">
								<input type="text" name="bundle_quantity" id="bundle_quantity" data-required="1" class="form-control" /> 
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn dark btn-outline" data-dismiss="modal" >Close</button>
					<button type="submit" class="btn green" href="#ajax" >Save changes</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


<!-- /.modal -->

@endsection


@section('buttom_scripts')

	<!-- BEGIN PAGE SCRIPTS -->
	{{-- <script src="{{ asset('packages/datatables/datatables.min.js') }}" type="text/javascript"></script> --}}
	<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="{{ asset('packages/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
	<script src="{{ asset('packages/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/pages/joborder.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/pages/bundles.js') }}" type="text/javascript"></script>
	<!-- END PAGE SCRIPTS -->

@stop