@extends('layout.layout')

@section('title')

@stop

@section('head')

<link href="{{ asset('packages/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/select2/css/select2-bootstrap.min.css" rel="stylesheet') }}" type="text/css" />
<link href="{{ asset('packages/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('packages/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('sidebar')
	@include('sidemenu.joborder')
@stop

@section('content')

<div class="page-head">
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-dark">
						<i class="icon-settings font-dark"></i>
						{{-- <span class="caption-subject bold uppercase"> Job Order Management</span> --}}
					</div>
				</div>
				<div class="portlet-body">
					<div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="{{ URL::to('/admin/joborder/add') }}" id="sample_editable_1_new" class="btn sbold green"> 
                                    	Add New
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
					<table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
						<thead>
							<tr>
								<th> Job Order # </th>
								<th> Client </th>
								<th> Status </th>
								<th> Volume </th>
								<th> Action </th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
			<!-- END EXAMPLE TABLE PORTLET-->
		</div>
	</div>

</div>

{{-- MODALS --}}
<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Confirmation</h4>
			</div>
			<div class="modal-body">
				<p> Would you like to continue with some arbitrary task? </p>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn dark btn-outline">Cancel</button>
				<button type="button" data-dismiss="modal" class="btn green">Continue Task</button>
			</div>
		</div>
	</div>
</div>

@endsection


@section('buttom_scripts')

<!-- BEGIN PAGE SCRIPTS -->
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{{ asset('packages/select2/js/select2.full.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('packages/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
<script src="{{ asset('packages/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/joborder.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/pages/joborder_list.js') }}" type="text/javascript"></script>
<!-- END PAGE SCRIPTS -->

@stop