<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/login', function () {
    return view('auth/login');
});

Route::group(array('middleware' => 'auth','prefix'=>'admin'), function(){

	Route::get('/', function () {
    	return view('joborder/index');
	});

	Route::group(array('prefix'=>'products'), function(){

		Route::get('/list', function () {
    		return view('products/index');
		});

		Route::get('/edit', function () {
    		return view('products/edit');
		});

		Route::get('/add', function () {
    		return view('products/add');
		});

		Route::post('/add', 'Auth\ProductController@create');

		Route::group(array('prefix'=>'operation'), function(){

			Route::get('/', function () {
    			return view('products/operation');
			});

		});


	});

	Route::group(array('prefix'=>'joborder'), function(){

		Route::get('/', function () {
    		return view('joborder/index');
		});

		Route::get('/detail', function () {
    		return view('joborder/details');
		});

		Route::get('/edit', function () {
    		return view('joborder/edit');
		});

		Route::get('/add', function () {
    		return view('joborder/add');
		});

		Route::get('/detail/bundle', function () {
    		return view('joborder/bundles');
		});

		Route::get('/detail/bundle/operations', function () {
    		return view('joborder/operations');
		});

	});

	Route::group(array('prefix'=>'client'), function(){

		Route::get('/', function () {
    		return view('clients/index');
		});

		Route::get('/edit', function () {
    		return view('clients/edit');
		});

		Route::get('/add', function () {
    		return view('clients/add');
		});

	});

	Route::group(array('prefix'=>'employee'), function(){

		Route::get('/', function () {
    		return view('employee/index');
		});

		Route::get('/edit', function () {
    		return view('employee/edit');
		});

		Route::get('/add', function () {
    		return view('employee/add');
		});

	});

});

Route::group(array('prefix'=>'auth'), function(){
	
	Route::post(	'/', 				'API\AuthController@create');
	Route::delete(	'/', 				'API\AuthController@delete');
	Route::get(	'/logout', 				'API\AuthController@delete');

});

Route::group(array('prefix'=>'api'), function(){

	Route::group(array('prefix'=>'auth'), function(){
	
		Route::get('/', 				'API\UserController@show');
		Route::post('/', 				'API\UserController@create');
		Route::put('/', 				'API\UserController@update');
		Route::put('/changepassword', 	'API\UserController@updatePassword');
		Route::delete('/', 				'API\UserController@delete');
	
	});

	Route::group(array('prefix'=>'users'), function(){
	
		Route::get('/', 				'API\UserController@show');
		Route::post('/', 				'API\UserController@create');
		Route::post('/completeinfo',	'API\UserController@createComplete');
		Route::put('/', 				'API\UserController@update');
		Route::put('/changepassword', 	'API\UserController@updatePassword');
		Route::delete('/', 				'API\UserController@delete');

		Route::group(array('prefix'=>'type'), function(){
			Route::get('/', 				'API\UserTypeController@show');
		});

		Route::group(array('prefix'=>'personal'), function(){

			Route::get('/', 		'API\PersonalInfoController@show');
			Route::post('/', 		'API\PersonalInfoController@create');
			Route::put('/', 		'API\PersonalInfoController@update');
	
		});

		Route::group(array('prefix'=>'goverment'), function(){

			Route::get('/', 		'API\GovermentInfoController@show');
			Route::post('/', 		'API\GovermentInfoController@create');
			Route::put('/', 		'API\GovermentInfoController@update');
	
		});
		
	});

	Route::group(array('prefix'=>'products'), function(){
	
		Route::get('/', 	'API\ProductController@show');
		Route::post('/', 	'API\ProductController@create');
		Route::put('/', 	'API\ProductController@update');

		Route::group(array('prefix'=>'type'), function(){
	
			Route::get('/', 	'API\ProductTypeController@show');
			Route::post('/', 	'API\ProductTypeController@create');
			Route::put('/', 	'API\ProductTypeController@update');
		
		});

		Route::group(array('prefix'=>'color'), function(){
	
			Route::get('/', 	'API\ProductColorController@show');
			Route::post('/', 	'API\ProductColorController@create');
			Route::put('/', 	'API\ProductColorController@update');
		
		});

		Route::group(array('prefix'=>'size'), function(){
	
			Route::get('/', 	'API\ProductSizeController@show');
			Route::post('/', 	'API\ProductSizeController@create');
			Route::put('/', 	'API\ProductSizeController@update');
		
		});

		Route::group(array('prefix'=>'operations'), function(){
	
			Route::get('/', 	'API\ProductOperationController@show');
			Route::post('/', 	'API\ProductOperationController@create');
			Route::put('/', 	'API\ProductOperationController@update');
			Route::delete('/', 	'API\ProductOperationController@delete');
		
		});
	
	});

	Route::group(array('prefix'=>'joborder'), function(){

		Route::get('/', 		'API\JobOrderController@show');
		Route::post('/', 		'API\JobOrderController@create');
		Route::put('/', 		'API\JobOrderController@update');

		Route::group(array('prefix'=>'details'), function(){
	
			Route::get('/', 	'API\JobOrderDetailController@show');
			Route::post('/', 	'API\JobOrderDetailController@create');
			Route::put('/', 	'API\JobOrderDetailController@update');
			Route::delete('/', 	'API\JobOrderDetailController@delete');

			Route::group(array('prefix'=>'bundle'), function(){
	
				Route::get('/', 	'API\JobOrderBundleController@show');
				Route::post('/', 	'API\JobOrderBundleController@create');
				Route::post('/withoperations', 	'API\JobOrderBundleController@createWithOperations');
				Route::put('/', 	'API\JobOrderBundleController@update');
				Route::delete('/', 	'API\JobOrderBundleController@delete');

				Route::group(array('prefix'=>'operations'), function(){
	
					Route::get('/', 	'API\JobOrderBundleOperationsController@show');
					Route::post('/', 	'API\JobOrderBundleOperationsController@create');
					Route::put('/', 	'API\JobOrderBundleOperationsController@update');
					Route::delete('/', 	'API\JobOrderBundleOperationsController@delete');
				
				});
			
			});
		
		});

		// CLEAN THIS BLOCK OF CODE CLEAN UP
		Route::group(array('prefix'=>'development'), function(){
	
			Route::get('/', 	'API\JobOrderDevelopmentController@show');
			Route::post('/', 	'API\JobOrderDevelopmentController@create');
			Route::put('/', 	'API\JobOrderDevelopmentController@update');
			Route::delete('/', 	'API\JobOrderDevelopmentController@delete');
		
		});
		// END CLEAN THIS BLOCK OF CODE CLEAN UP

		Route::group(array('prefix'=>'status'), function(){
			Route::get('/', 	'API\JobOrderStatusController@show');
		});

	});

	Route::group(array('prefix'=>'client'), function(){

		Route::get('/', 		'API\ClientController@show');
		Route::post('/', 		'API\ClientController@create');
		Route::put('/', 		'API\ClientController@update');

	});

});