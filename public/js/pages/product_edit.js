var FormValidation = function () {

    // basic validation
    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#form_sample_1');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }
                },
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    type: {
                        required: true
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },

                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                    },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                    },

                    success: function (label) {
                        label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                    },

                    submitHandler: function (form) {
                        var formURL = $('#form_sample_1').attr("action");

                        $.ajax({
                          url: formURL,
                          type: 'put',
                          data: $('#form_sample_1').serialize(),
                          success: function(data) {
                            console.log(data);
                            error1.hide();
                            success1.show();
                        // window.location.href = "../../../admin/products/list";
                    }
                });
                    return false; // kill page refresh


                    
                }
            });


        }

        return {

        initProduct: function (els) {
            var ids = ReturnParam('id');
            /* Send the data */
            $.ajax({
                url:"../../../../../api/products",
                contentType: 'application/json; charset=utf-8',
                data: { id: ids },
                type: "get",
                dataType: 'json',
                success: function (json) {
                    $('#id').val(ids);
                    $('#name_product').val(json.data.name);
                    $('#description').val(json.data.description);
                    $("#type").val(json.data.type_id);
                },
                error: function () {
                    console.log('error');
                }
            });
        },

        //main function to initiate the module
        init: function () {
            handleValidation1();
        }

    };

}();

$(document).ready(function() {

    window.ReturnParam = function(sParam){
        //Hakuna matata ^_^
        var sPageURL = window.location.href;
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            return sParameterName[1];
        }
    };

} );// END Ducement Ready

jQuery(document).ready(function() {
    FormValidation.init();
    FormValidation.initProduct();
});