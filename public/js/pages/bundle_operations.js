var TableDatatablesManaged = function () {

    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            var form1 = $('#form_sample_1');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }
                },
                rules: {
                    product_id: {
                        required: true
                    },
                    color_id: {
                        required: true
                    },
                    size_id: {
                        required: true
                    },
                    bundle_quantity: {
                        required: true
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },

                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    var formURL = $('#form_sample_1').attr("action");

                    $.ajax({
                      url: formURL,
                      type: 'post',
                      data: $('#form_sample_1').serialize(),
                      success: function(data) {
                       if(data.result == 'Failed')
                        {
                            toastr.error(data.result, data.message)
                        }else{
                            toastr.success(data.result, data.message)
                        }
            
                        $('#add').modal('toggle');
                        $('#sample_1').DataTable().ajax.reload();
                        // window.location.href = "../../../admin/joborder";
                      }
                    });
                    return false; // kill page refresh
                }
            });
    }

    var initTable1 = function () {
        
        var jo = ReturnParam('jo');
        var batch = ReturnParam('batch');
        var bundle = ReturnParam('bundle');
        
        // Job Order
        $.ajax({
            url:"../../../../../api/joborder",
            contentType: 'application/json; charset=utf-8',
            data: { id: jo },
            type: "get",
            dataType: 'json',
            success: function (json) {
                $('#c_jo').text(json.data.c_jo);
                $('#c_jo2').text(json.data.c_jo);
                $("#volume").html(json.data.volume);
                
                $('#totalBundle').text(json.data.total_bundles);
            },
            error: function () {
                console.log('error');
            }
        });

        // Batch
        $.ajax({
            url:"../../../../../api/joborder/details",
            contentType: 'application/json; charset=utf-8',
            data: { id: batch },
            type: "get",
            dataType: 'json',
            success: function (json) {
                $("#product").html(json.data.product.name);
                $('#batch').text(json.data.batch);
                $("#batch_volume").html(json.data.batch_volume);
            },
            error: function () {
                console.log('error');
            }
        });

        function pad(n, width, z) {
          z = z || '0';
          n = n + '';
          return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }

        // Bundle
        $.ajax({
            url:"../../../../../api/joborder/details/bundle",
            contentType: 'application/json; charset=utf-8',
            data: { id: bundle },
            type: "get",
            dataType: 'json',
            success: function (json) {
                $('#batch_code').html(batch_code);
                $('#batch_code_title').html(batch_code);
                $('#color').text(json.data.color.name);
                $('#size').text(json.data.size.name);
                $('#size2').text(json.data.size.name);
                $('#size_top').text(json.data.size.name);
                
                var batchNum = pad(json.data.bundle_number, 3);
                $('#bundle_number').text(batchNum);
                $('#ticket').text(batchNum);
                $("#bundle_quantity").html(json.data.bundle_quantity);
            },
            error: function () {
                console.log('error');
            }
        });

        $.ajax({
            url:"../../../../../api/joborder/details/bundle/operations",
            contentType: 'application/json; charset=utf-8',
            data: { bundle_id: bundle },
            type: "get",
            dataType: 'json',
            success: function (json) {
                var content = '';
                var content2 = '';

                var l = json.data.length;

                var res = l/3;
                var ceil = Math.ceil(res);
                var mul = ceil*3;
                var additionalCell = mul - l;

                for (i = 0; i < additionalCell; i++) { 
                    content2 = content2 + '<div class="oprn_code">'
                        + '     <span> <i class="fa fa-bars"></i> ' + 'BLANK' + '</span><br>'
                        + '</div>';
                }


                 $.each(json.data, function (idx, operations) {
                    
                    content = content + '<div class="oprn_box">'
                        + '     <span> ' + operations.operation.step_number + '. ' + operations.operation.name +  '</span><br>'
                        + '     <span>Name :</span>'
                        + '</div>';

                    content2 = content2 + '<div class="oprn_code">'
                        + '     <span><i class="fa fa-bars"></i> ' + operations.operation.step_number + '. ' + operations.operation.name + ' - ' + operations.operation_code + '</span><br>'
                        // + '     <div class="codeimg">'
                        + '         <svg id="barcode'+operations.id+'"></svg>'
                        // + '     <div>'
                        + '</div>'
                        + '<script>'
                        + 'JsBarcode("#barcode'+operations.id+'", "' + operations.operation_code +'", { height: 15,width: 1, displayValue: false } );'
                        + '</script>';

                });

                

                $("#oprn_cont").html(content);
                $("#oprn_code_cont").html(content2);

                $("#oprn_code_cont").sortable({
                    connectWith: ".oprn_code",
                    items: ".oprn_code", 
                    opacity: 0.8,
                    handle : 'span i',
                    coneHelperSize: true,
                    placeholder: 'portlet-sortable-placeholder',
                    forcePlaceholderSize: true,
                    tolerance: "pointer",
                    helper: "clone",
                    tolerance: "pointer",
                    forcePlaceholderSize: !0,
                    helper: "clone",
                    cancel: ".portlet-sortable-empty, .portlet-fullscreen", // cancel dragging if portlet is in fullscreen mode
                    revert: 250, // animation in milliseconds
                    update: function(b, c) {
                        if (c.item.prev().hasClass("portlet-sortable-empty")) {
                            c.item.prev().before(c.item);
                        }                    
                    }
                });
            },
            error: function () {
                console.log('error');
            }
        });
    }

    var handleAutoComplete = function() {

        $('#product').autocomplete({
            source: function( request, response ) {
                $( "#product" ).addClass( "loading" );
                $.ajax({
                    url: '../../../../../api/products',
                    dataType: "json",
                    data: {
                        key: request.term
                    },
                    success: function (result) {
                        response($.map(result.data, function (item) {
                            $('#product_id').val(item.id);
                            $( "#product" ).removeClass( "loading" );
                            console.log(result);
                            return {
                                label: item.name,
                                value: item.name
                            };

                        }));

                    }
                });


            },
            minLength: 1,
            appendTo: "#form_sample_1"
        });

        $('#color').autocomplete({
            source: function( request, response ) {
                $( "#color" ).addClass( "loading" );
                $.ajax({
                    url: '../../../../../api/products/color',
                    dataType: "json",
                    data: {
                        key: request.term
                    },
                    success: function (result) {
                        response($.map(result.data, function (item) {
                            $('#color_id').val(item.id);
                            $( "#color" ).removeClass( "loading" );
                            return {
                                label: item.name,
                                value: item.name
                            };

                        }));

                    }
                });


            },
            minLength: 1,
            appendTo: "#form_sample_1"
        });

        $('#size').autocomplete({
            source: function( request, response ) {
                $( "#size" ).addClass( "loading" );
                $.ajax({
                    url: '../../../../../api/products/size',
                    dataType: "json",
                    data: {
                        key: request.term
                    },
                    success: function (result) {
                        response($.map(result.data, function (item) {
                            $('#size_id').val(item.id);
                            $( "#size" ).removeClass( "loading" );
                            return {
                                label: item.name,
                                value: item.name
                            };

                        }));

                    }
                });


            },
            minLength: 1,
            appendTo: "#form_sample_1"
        });

    }

    var handlePrint = function() {
        // $("#mydiv").printThis({
        //     debug: false,               // show the iframe for debugging
        //     importCSS: true,            // import page CSS
        //     printContainer: true,       // grab outer container as well as the contents of the selector
        //     pageTitle: "Bundle Ticket",              // add title to print page
        //     removeInline: false,        // remove all inline styles from print elements
        //     printDelay: 333,            // variable print delay; depending on complexity a higher value may be necessary
        //     header: null,               // prefix to html
        //     footer: null,               // postfix to html
        //     base: true,                  // preserve the BASE tag, or accept a string for the URL
        //     formValues: true,            // preserve input/form values
        //     canvas: false,               // copy canvas elements (experimental)
        //     doctypeString: "..."        // enter a different doctype for older markup
        // });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
            handleValidation1();
            handlePrint();
            // handleAutoComplete();


        }

    };

}();


if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        TableDatatablesManaged.init();
    });
}

$(document).ready(function() {

    window.PrintElem = function(){
        $("#mydiv").printThis({
            debug: false,               // show the iframe for debugging
            importCSS: true,            // import page CSS
            printContainer: true,       // grab outer container as well as the contents of the selector
            pageTitle: "Bundle Ticket",              // add title to print page
            removeInline: false,        // remove all inline styles from print elements
            printDelay: 333,            // variable print delay; depending on complexity a higher value may be necessary
            header: null,               // prefix to html
            footer: null,               // postfix to html
            base: true,                  // preserve the BASE tag, or accept a string for the URL
            formValues: true,            // preserve input/form values
            canvas: false,               // copy canvas elements (experimental)
            doctypeString: "..."        // enter a different doctype for older markup
        });
    };


} );// END Ducement Ready




