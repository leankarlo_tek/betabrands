var TableDatatablesManaged = function () {

    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            var form1 = $('#form_sample_1');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }
                },
                rules: {
                    product_id: {
                        required: true
                    },
                    color_id: {
                        required: true
                    },
                    size_id: {
                        required: true
                    },
                    bundle_quantity: {
                        required: true
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },

                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    var formURL = $('#form_sample_1').attr("action");
                    $('#ajax').modal('show');
                    $.ajax({
                      url: formURL,
                      type: 'post',
                      data: $('#form_sample_1').serialize(),
                      success: function(data) {
                       if(data.result == false)
                        {
                            toastr.error(data.result, data.message)
                        }else{
                            toastr.success(data.result, data.message)
                        }
            
                        $('#add').modal('toggle');
                        $('#sample_1').DataTable().ajax.reload();
                        $('#ajax').modal('toggle');
                        // window.location.href = "../../../admin/joborder";
                      }
                    });
                    return false; // kill page refresh
                }
            });
    }

    var handleValidationColor = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            var form1 = $('#form_color');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }
                },
                rules: {
                    product_id: {
                        required: true
                    },
                    color_id: {
                        required: true
                    },
                    size_id: {
                        required: true
                    },
                    bundle_quantity: {
                        required: true
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },

                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    var formURL = $('#form_color').attr("action");
                    $('#ajax').modal('show');
                    $.ajax({
                      url: formURL,
                      type: 'post',
                      data: $('#form_color').serialize(),
                      success: function(data) {
                       if(data.result == false)
                        {
                            toastr.error(data.result, data.message);
                        }else{
                            toastr.success(data.result, data.message);
                            handleColorSelect();
                        }
            
                        $('#addColor').modal('toggle');
                        $('#ajax').modal('toggle');
                      }
                    });
                    return false; // kill page refresh
                }
            });
    }

    var handleValidationSize = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            var form1 = $('#form_size');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }
                },
                rules: {
                    product_id: {
                        required: true
                    },
                    color_id: {
                        required: true
                    },
                    size_id: {
                        required: true
                    },
                    bundle_quantity: {
                        required: true
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },

                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    var formURL = $('#form_size').attr("action");
                    $('#ajax').modal('show');
                    $.ajax({
                      url: formURL,
                      type: 'post',
                      data: $('#form_size').serialize(),
                      success: function(data) {
                       if(data.result == false)
                        {
                            toastr.error(data.result, data.message);
                        }else{
                            toastr.success(data.result, data.message);
                            handleSizeSelect();
                        }
            
                        $('#addSize').modal('toggle');
                        $('#ajax').modal('toggle');
                        // window.location.href = "../../../admin/joborder";
                      }
                    });
                    return false; // kill page refresh
                }
            });
    }

    var initTable1 = function () {
        
        var jo_id = ReturnParam('jo_id');
        var detail_id = ReturnParam('jo_detail_id');
        console.log(jo_id);
        $('#jo_detail_id').val(detail_id);

        $.ajax({
            url:"../../../../../api/joborder",
            contentType: 'application/json; charset=utf-8',
            data: { id: jo_id },
            type: "get",
            dataType: 'json',
            success: function (json) {
                $('#clientName').text(json.data.client.name);
                $('#totalBundle').text(json.data.total_bundles);
                $('#status').html(json.data.status.name);
                $("#volume").html(json.data.volume);
            },
            error: function () {
                console.log('error');
            }
        });

        $.ajax({
            url:"../../../../../api/joborder/details",
            contentType: 'application/json; charset=utf-8',
            data: { id: detail_id },
            type: "get",
            dataType: 'json',
            success: function (json) {
                $('#batch').text(json.data.batch);
                $('#product').text(json.data.product.name);
                // $('#started_at').text(json.data.started_at);
                // $('#ended_at').text(json.data.ended_at);
                $('#status_batch').html(json.data.status.name);
                $("#batch_volume").html(json.data.batch_volume);
            },
            error: function () {
                console.log('error');
            }
        });

        $('#sample_1').DataTable( {
            "paginate": false,
            "ajax": "../../../../../api/joborder/details/bundle?jo_detail_id="+detail_id,
            "columns": [
                { "data": "size.name" },
                { "data": "color.name" },
                { "data": "bundle_quantity" },
                { "data": "status.name" },
                {
                    sortable: false,
                    "render": function ( data, type, result, meta ) {
                        var content =       '<div class="btn-group">'
                                        +   '    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions'
                                        +   '        <i class="fa fa-angle-down"></i>'
                                        +   '    </button>'
                                        +   '    <ul class="dropdown-menu" role="menu">'
                                        +   '        <li>'
                                        +   '           <a href="../../../../../admin/joborder/detail/bundle/operations?batch=' + detail_id + '&jo='+jo_id+'&bundle='+ result.id +'  ">'
                                        +   '                <i class="icon-docs"></i> Bundle Status' 
                                        +   '           </a>'
                                        +   '        </li>'
                                        +   '    </ul>'
                                        +   '</div>';
                        return content;
                    }
                }
            ]
        } );
    }

    var handleAutoComplete = function() {

        $('#product').autocomplete({
            source: function( request, response ) {
                $( "#product" ).addClass( "loading" );
                $.ajax({
                    url: '../../../../../api/products',
                    dataType: "json",
                    data: {
                        key: request.term
                    },
                    success: function (result) {
                        response($.map(result.data, function (item) {
                            $( "#product" ).removeClass( "loading" );
                            console.log(result);
                            return {
                                label: item.name,
                                value: item.name,
                                key: item.id
                            };

                        }));

                    }
                });


            },
            select: function(event, ui) {
                $("#product_id").val(ui.item.key);  // ui.item.value contains the id of the selected label
            },
            minLength: 1,
            appendTo: "#form_sample_1"
        });

        $('#color').autocomplete({
            source: function( request, response ) {
                $( "#color" ).addClass( "loading" );
                $.ajax({
                    url: '../../../../../api/products/color',
                    dataType: "json",
                    data: {
                        key: request.term
                    },
                    success: function (result) {
                        response($.map(result.data, function (item) {
                            $( "#color" ).removeClass( "loading" );
                            return {
                                label: item.name,
                                value: item.name,
                                key: item.id
                            };

                        }));

                    }
                });


            },
            select: function(event, ui) {
                $("#color_id").val(ui.item.key);  // ui.item.value contains the id of the selected label
            },
            minLength: 1,
            appendTo: "#form_sample_1"
        });

        $('#size').autocomplete({
            source: function( request, response ) {
                $( "#size" ).addClass( "loading" );
                $.ajax({
                    url: '../../../../../api/products/size',
                    dataType: "json",
                    data: {
                        key: request.term
                    },
                    success: function (result) {
                        response($.map(result.data, function (item) {
                            $( "#size" ).removeClass( "loading" );
                            return {
                                label: item.name,
                                value: item.name,
                                key: item.id
                            };

                        }));

                    }
                });


            },
            select: function(event, ui) {
                $("#size_id").val(ui.item.key);  // ui.item.value contains the id of the selected label
            },
            minLength: 1,
            appendTo: "#form_sample_1"
        });

    }

    var handleColorSelect = function() {
        var res = [];

        $.ajax({
            url:"../../../../../api/products/color",
            contentType: 'application/json; charset=utf-8',
            type: "get",
            dataType: 'json',
            success: function (result) {
                $.each(result.data, function (idx, color) {
                    res.push({"id": color.id, "text": color.name});
                });

                $("#color_id").select2({
                    data: res,
                    placeholder: "Select Color",
                    allowClear: true,
                    width: '100%'
                })

            },
            error: function () {
                console.log('error');
            }
        });
    }

    var handleSizeSelect = function() {
        var res = [];

        $.ajax({
            url:"../../../../../api/products/size",
            contentType: 'application/json; charset=utf-8',
            type: "get",
            dataType: 'json',
            success: function (result) {
                $.each(result.data, function (idx, size) {
                    res.push({"id": size.id, "text": size.name});
                });

                $("#size_id").select2({
                    data: res,
                    placeholder: "Select Color",
                    allowClear: true,
                    width: '100%'
                })

            },
            error: function () {
                console.log('error');
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTable1();
            handleValidation1();
            handleValidationColor();
            handleValidationSize();
            handleAutoComplete();
            handleColorSelect();
            handleSizeSelect();
        }

    };

}();


if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        TableDatatablesManaged.init();
    });
}




