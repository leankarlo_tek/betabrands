var ActionManagement = function () {

    var handleValidationColor = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            var form1 = $('#form_color');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }
                },
                rules: {
                    product_id: {
                        required: true
                    },
                    color_id: {
                        required: true
                    },
                    size_id: {
                        required: true
                    },
                    bundle_quantity: {
                        required: true
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },

                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    var formURL = $('#form_color').attr("action");
                    $('#ajax').modal('show');
                    $.ajax({
                      url: formURL,
                      type: 'post',
                      data: $('#form_color').serialize(),
                      success: function(data) {
                       if(data.result == false)
                        {
                            toastr.error(data.result, data.message);
                        }else{
                            toastr.success(data.result, data.message);
                            handleColorSelect();
                        }
            
                        $('#addColor').modal('toggle');
                        $('#ajax').modal('toggle');
                      }
                    });
                    return false; // kill page refresh
                }
            });
    }

    var handleValidationSize = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            var form1 = $('#form_size');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }
                },
                rules: {
                    product_id: {
                        required: true
                    },
                    color_id: {
                        required: true
                    },
                    size_id: {
                        required: true
                    },
                    bundle_quantity: {
                        required: true
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },

                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    var formURL = $('#form_size').attr("action");
                    $('#ajax').modal('show');
                    $.ajax({
                      url: formURL,
                      type: 'post',
                      data: $('#form_size').serialize(),
                      success: function(data) {
                       if(data.result == false)
                        {
                            toastr.error(data.result, data.message);
                        }else{
                            toastr.success(data.result, data.message);
                            handleSizeSelect();
                        }
            
                        $('#addSize').modal('toggle');
                        $('#ajax').modal('toggle');
                        // window.location.href = "../../../admin/joborder";
                      }
                    });
                    return false; // kill page refresh
                }
            });
    }

    var handleColorSelect = function() {
        var res = [];

        $.ajax({
            url:"../../../../../api/products/color",
            contentType: 'application/json; charset=utf-8',
            type: "get",
            dataType: 'json',
            success: function (result) {
                $.each(result.data, function (idx, color) {
                    res.push({"id": color.id, "text": color.name});
                });

                $("#color_id").select2({
                    data: res,
                    placeholder: "Select Color",
                    allowClear: true,
                    width: '100%'
                })

            },
            error: function () {
                console.log('error');
            }
        });
    }

    var handleSizeSelect = function() {
        var res = [];

        $.ajax({
            url:"../../../../../api/products/size",
            contentType: 'application/json; charset=utf-8',
            type: "get",
            dataType: 'json',
            success: function (result) {
                $.each(result.data, function (idx, size) {
                    res.push({"id": size.id, "text": size.name});
                });

                $("#size_id").select2({
                    data: res,
                    placeholder: "Select Color",
                    allowClear: true,
                    width: '100%'
                })

            },
            error: function () {
                console.log('error');
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            handleValidationColor();
            handleValidationSize()
            handleColorSelect();
            handleSizeSelect();
        }

    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        ActionManagement.init();
        
    });
}

