var FormValidation = function () {

    // basic validation
    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#form_sample_1');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }
                },
                rules: {
                    client_id: {
                        required: true
                    },
                    volume: {
                        required: true
                    },
                    total_bundles: {
                        required: true
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },

                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    var formURL = $('#form_sample_1').attr("action");

                    $.ajax({
                      url: formURL,
                      type: 'post',
                      data: $('#form_sample_1').serialize(),
                      success: function(data) {
                        console.log(data);
                        error1.hide();
                        success1.show();
                        // window.location.href = "../../../admin/joborder";
                      }
                    });
                    return false; // kill page refresh
                }
            });


    }

    var handleAutoComplete = function() {

        $('#client').autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: '../../../../../api/client',
                    dataType: "json",
                    data: {
                        key: request.term
                    },
                    success: function (result) {
                        response($.map(result.data, function (item) {
                            $('#client_id').val(item.id);
                            return {
                                label: item.name,
                                value: item.name
                            };
                        }));

                    }
                });
            },
            minLength: 1
        });

    }

    return {
        //main function to initiate the module
        init: function () {

            handleValidation1();
            handleAutoComplete();

        },

        initJobOrder: function (els) {
            var ids = ReturnParam('id');


            // Fill in status Combo Box
            $.ajax({
                url:"../../../../../api/joborder/status",
                contentType: 'application/json; charset=utf-8',
                type: "get",
                dataType: 'json',
                success: function (json) {
                    var options = '';
                    $.each(json.data, function (idx, type) {
                        options = options + '<option value="'+type.id+'">'+type.name+'</option>';
                    });
                    $('#status').html(options);
                },
                error: function () {
                    console.log('error');
                }
            });


            /* Send the data */
            $.ajax({
                url:"../../../../../api/joborder",
                contentType: 'application/json; charset=utf-8',
                data: { id: ids },
                type: "get",
                dataType: 'json',
                success: function (json) {
                    $('#id').val(ids);
                    $('#client').val(json.data.client.name);
                    $('#client_id').val(json.data.client.id);
                    $('#volume').val(json.data.volume);
                    $("#total_bundles").val(json.data.total_bundles);
                    $("#status").val(json.data.status.id).change();
                },
                error: function () {
                    console.log('error');
                }
            });
        }

    };

}();

$(document).ready(function() {

    window.ReturnParam = function(sParam){
        //Hakuna matata ^_^
        var sPageURL = window.location.href;
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            return sParameterName[1];
        }
    };

} );// END Ducement Ready

jQuery(document).ready(function() {
    FormValidation.init();
    FormValidation.initJobOrder()
});