var TableDatatablesManaged = function () {

    var initTable1 = function () {
        console.log('test');
        $('#sample_1').DataTable( {
                "ajax": "../../../../../api/products",
                "columns": [
                    { "data": "name" },
                    { "data": "description" },
                    { "data": "type.name" },
                    {
                        sortable: false,
                        "render": function ( data, type, product, meta ) {
                            var content =       '<div class="btn-group">'
                                            +   '    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions'
                                            +   '        <i class="fa fa-angle-down"></i>'
                                            +   '    </button>'
                                            +   '    <ul class="dropdown-menu" role="menu">'
                                            +   '        <li>'
                                            +   '           <a href="../../../../../admin/products/edit?id=' + product.id + '">'
                                            +   '                <i class="icon-docs"></i> Edit' 
                                            +   '           </a>'
                                            +   '        </li>'
                                            +   '        <li>'
                                            +   '            <a data-toggle="modal" href="../../../../../admin/products/operation?id=' + product.id + '">'
                                            +   '                <i class="icon-tag"></i> Process </a>'
                                            +   '        </li>'
                                            +   '    </ul>'
                                            +   '</div>';
                            

                            return content;
                        }
                    }
                ]
            } );
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
        }

    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        TableDatatablesManaged.init();
        
    });
}

