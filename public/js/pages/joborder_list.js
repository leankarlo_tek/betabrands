var JOTableManagement = function () {

    var initTable = function () {
        console.log('load');
        $('#sample_1').DataTable( {
                "ajax": "../../../../../api/joborder",
                "columns": [
                    { "data": "c_jo" },
                    { "data": "client.name" },
                    { "data": "status.name" },
                    { "data": "volume" },
                    {
                        sortable: false,
                        "render": function ( data, type, product, meta ) {
                            var content =       '<div class="btn-group">'
                                            +   '    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions'
                                            +   '        <i class="fa fa-angle-down"></i>'
                                            +   '    </button>'
                                            +   '    <ul class="dropdown-menu" role="menu">'
                                            +   '        <li>'
                                            +   '           <a href="../../../../../admin/joborder/edit?id=' + product.id + '">'
                                            +   '                <i class="icon-docs"></i> Edit' 
                                            +   '           </a>'
                                            +   '        </li>'
                                            +   '        <li>'
                                            +   '           <a href="../../../../../admin/joborder/detail?id=' + product.id + '">'
                                            +   '                <i class="icon-docs"></i> View More' 
                                            +   '           </a>'
                                            +   '        </li>'
                                            +   '    </ul>'
                                            +   '</div>';
                            return content;
                        }
                    }
                ]
            } );
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable();
        }

    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        JOTableManagement.init();
        
    });
}

