var TableDatatablesManaged = function () {

    var initTable1 = function () {
        console.log('test');
        $('#sample_1').DataTable( {
            "ajax": "../../../../../api/users",
            "columns": [
                { "data": "username" },
                { "data": "email" },
                { "data": "position" },
                { "data": "lastname"},
                { "data": "firstname"},
                {
                    sortable: false,
                    "render": function ( data, type, user, meta ) {
                        var content =       '<div class="btn-group">'
                                        +   '    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions'
                                        +   '        <i class="fa fa-angle-down"></i>'
                                        +   '    </button>'
                                        +   '    <ul class="dropdown-menu" role="menu">'
                                        +   '        <li>'
                                        +   '           <a href="../../../../../admin/employee/edit?id=' + user.id + '">'
                                        +   '                <i class="icon-docs"></i> Edit' 
                                        +   '           </a>'
                                        +   '        </li>'
                                        +   '    </ul>'
                                        +   '</div>';
                        return content;
                    }
                }
            ]
        } );
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTable1();
        }
    };
}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        TableDatatablesManaged.init();
        
    });
}

