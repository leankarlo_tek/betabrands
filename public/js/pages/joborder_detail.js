var TableDatatablesManaged = function () {

    var handleValidation1 = function() {
        // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            var form1 = $('#form_sample_1');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }
                },
                rules: {
                    client_id: {
                        required: true
                    },
                    volume: {
                        required: true
                    },
                    total_bundles: {
                        required: true
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },

                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    var formURL = $('#form_sample_1').attr("action");

                    $.ajax({
                      url: formURL,
                      type: 'post',
                      data: $('#form_sample_1').serialize(),
                      success: function(data) {
                       if(data.result == false)
                        {
                            toastr.error(data.result, data.message)
                        }else{
                            toastr.success(data.result, data.message)
                        }
            
                        $('#add').modal('toggle');
                        $('#ajax').modal('toggle');
                        $('#sample_1').DataTable().ajax.reload();
                        // window.location.href = "../../../admin/joborder";
                      }
                    });
                    return false; // kill page refresh
                }
            });
    }

    var initTable1 = function () {
        
        var id = ReturnParam('id');
        $('#jo_id').val(id);

        $.ajax({
            url:"../../../../../api/joborder",
            contentType: 'application/json; charset=utf-8',
            data: { id: id },
            type: "get",
            dataType: 'json',
            success: function (json) {
                $('#clientName').text(json.data.client.name);
                $('#totalBundle').text(json.data.total_bundles);
                $('#status').html(json.data.status.name);
                $("#volume").html(json.data.volume);
            },
            error: function () {
                console.log('error');
            }
        });

        $('#sample_1').DataTable( {
                "paginate": false,
                "ajax": "../../../../../api/joborder/details?jo_id="+id,
                "columns": [
                    { "data": "batch" },
                    { "data": "product.name" },
                    { "data": "batch_volume" },
                    { "data": "status.name" },
                    {
                        sortable: false,
                        "render": function ( data, type, detail, meta ) {
                            var content =       '<div class="btn-group">'
                                            +   '    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions'
                                            +   '        <i class="fa fa-angle-down"></i>'
                                            +   '    </button>'
                                            +   '    <ul class="dropdown-menu" role="menu">'
                                            +   '        <li>'
                                            +   '           <a href="../../../../../admin/joborder/detail/bundle?jo_detail_id=' + detail.id + '&jo_id='+id+'">'
                                            +   '                <i class="icon-docs"></i> Batch Status' 
                                            +   '           </a>'
                                            +   '        </li>'
                                            +   '    </ul>'
                                            +   '</div>';
                            return content;
                        }
                    }
                ]
            } );
    }

    var initSelect = function () {
        var res = [];

        $.ajax({
            url:"../../../../../api/products",
            contentType: 'application/json; charset=utf-8',
            type: "get",
            dataType: 'json',
            success: function (result) {
                $.each(result.data, function (idx, products) {
                    console.log(res);
                    res.push({"id": products.id, "text": products.name});
                });

                $("#product_id").select2({
                    data: res,
                    placeholder: "select products",
                    allowClear: true,
                    width: '100%'
                })

            },
            error: function () {
                console.log('error');
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
            handleValidation1();
            initSelect();
        }
    };
}();


if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        TableDatatablesManaged.init();
    });
}




