var TableDatatablesManaged = function () {

    var initTable1 = function () {
        
        var id = ReturnParam('id');
        var url = "../../../../../api/products/operations?product_id=" + id;
        $.ajax({
            url:"../../../../../api/products",
            contentType: 'application/json; charset=utf-8',
            data: { id: id },
            type: "get",
            dataType: 'json',
            success: function (json) {
                $('.caption-subject').html('NAME: '+json.data.name);

                $('#product_id').val(id);
            },
            error: function () {
                console.log('error');
            }
        });

        $('#sample_1').DataTable( {
                "ajax":url,
                "columns": [
                    { "data": "step_number" },
                    { "data": "name" },
                    { "data": "description" },
                    {
                        sortable: false,
                        "render": function ( data, type, operation, meta ) {
                            var content =       '<div class="btn-group">'
                                            +   '    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions'
                                            +   '        <i class="fa fa-angle-down"></i>'
                                            +   '    </button>'
                                            +   '    <ul class="dropdown-menu" role="menu">'
                                            +   '        <li>'
                                            +   '           <a data-toggle="modal" href="#edit" onclick="edit(' + operation.id + ')">'
                                            +   '                <i class="icon-docs"></i> Edit' 
                                            +   '           </a>'
                                            +   '        </li>'
                                            +   '        <li>'
                                            +   '           <a href="#messageAlert" onclick="alert('+operation.id+',\' Are you sure you want to REMOVE this operation? \', \'deleteOperation\' )" data-toggle="modal">'
                                            +   '                <i class="icon-tag"></i> Delete </a>'
                                            +   '        </li>'
                                            +   '    </ul>'
                                            +   '</div>';
                            return content;
                        }
                    }
                ]
            } );
    }

    var handleValidation1 = function() {
        var id = ReturnParam('id');
        var form1 = $('#form_sample_1');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
                select_multi: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected")
                }
            },
            rules: {
                name: {
                    minlength: 2,
                    required: true
                },
                description: {
                    minlength: 2,
                    required: true
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
                },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
                submitHandler: function (form) {
                    var formURL = $('#form_sample_1').attr("action");
                    $.ajax({
                        url: formURL,
                        type: 'PUT',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: $('#form_sample_1').serialize(),
                        success: function(data) {
                            if(data.result == 'Failed')
                            {
                                toastr.error(data.result, data.message)
                            }else{
                                toastr.success(data.result, data.message)
                            }
                
                            $('#add').modal('toggle');
                            $('#sample_1').DataTable().ajax.reload();
                        // window.location.href = "../../../admin/products/operation?id="+id;
                        }
                    });
                return false; // kill page refresh
                }
        });
    }

    var handleValidation2 = function() {
        var id = ReturnParam('id');
        var form1 = $('#form_sample_2');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
                select_multi: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected")
                }
            },
            rules: {
                name: {
                    minlength: 2,
                    required: true
                },
                step_number: {
                    minlength: 1
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
                },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
                submitHandler: function (form) {
                    var formURL = $('#form_sample_2').attr("action");
                    console.log($('#form_sample_2').serialize());
                    $.ajax({
                        url: formURL,
                        type: 'put',
                        data: $('#form_sample_2').serialize(),
                        success: function(json) {
                            if(json.result == 'Failed')
                            {
                                toastr.error(json.result, json.message)
                            }else{
                                toastr.success(json.result, json.message)
                            }
                            $('#edit').modal('toggle');
                            $('#sample_1').DataTable().ajax.reload();
                            window.location.href = "../../../admin/products/operation?id="+id;
                        }
                    });
                    // return false; // kill page refresh
                }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }

            initTable1();
            handleValidation1();
            handleValidation2();
        }

    };

}();

$(document).ready(function() {

    window.deleteOperation = function(id){
        $.ajax({
            url: "../../../../../api/products/operations",
            async: false,
            type: "delete",
            data: { id: id },
            success: function (data) {
                if(data.result == 'Failed')
                {
                    toastr.error(data.result, data.message)
                }else{
                    toastr.success(data.result, data.message)
                }
                
            },
            error: function () {
                var error_msg = Errorhandler('ErrorConn');
                toastr.error('ALERT', error_msg);
            }
        });
        $('#sample_1').DataTable().ajax.reload();
    }

    window.edit = function(id){
        $('#form_sample_2 #id').val();
        $('#form_sample_2 #name').val();
        $('#form_sample_2 #description').val();
        $('#form_sample_2 #step_number').val();

        $.ajax({
            url: "../../../../../api/products/operations",
            contentType: 'application/json; charset=utf-8',
            type: "get",
            data: { id: id },
            success: function (result) {
               $('#form_sample_2 #id').val(result.data.id);
               $('#form_sample_2 #name').val(result.data.name);
               $('#form_sample_2 #description').val(result.data.description);
               $('#form_sample_2 #step_number').val(result.data.step_number);
            },
            error: function () {
                var error_msg = Errorhandler('ErrorConn');
                toastr.error('ALERT', error_msg);
            }
        });
    }

} );// END Ducement Ready

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        TableDatatablesManaged.init();
        
    });
}

