var ClientTableManagement = function () {
    var initTable = function () {
        console.log('load');
        $('#sample_1').DataTable( {
            "ajax": "../../../../../api/client",
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "created_at" }
            ]
        } );



    }
    return {
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTable();
        }
    };
}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        ClientTableManagement.init();
    });
}

