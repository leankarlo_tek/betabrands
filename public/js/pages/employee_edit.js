var AddingEmployee = function () {

    var handleSpecialInput = function () {
        $('#birthday').datepicker({
            rtl: App.isRTL(),
            orientation: "left",
            autoclose: true,
            placeholder: '02/22/2017'
        });

        $("#country").select2({
            placeholder: "Select",
            allowClear: true,
            width: 'auto'
        });
    }

    var handleUserTypeSelect = function () {
        var res = [];
        $.ajax({
            url:"../../../../../api/users/type",
            contentType: 'application/json; charset=utf-8',
            type: "get",
            dataType: 'json',
            success: function (result) {
                $.each(result.data, function (idx, color) {
                    res.push({"id": color.id, "text": color.name});
                });
                $("#type").select2({
                    data: res,
                    placeholder: "Select Access Type",
                    allowClear: true,
                    width: '100%'
                })
            },
            error: function () {
                console.log('error');
            }
        });
    }

    var handleDataLoad = function () {
        var user_id = ReturnParam('id');
            $.ajax({
                url:"../../../../../api/users",
                contentType: 'application/json; charset=utf-8',
                data: { id: user_id },
                type: "get",
                dataType: 'json',
                success: function (json) {

                    $('#basic_form #id').val(user_id);
                    $('#basic_form #type').val(json.data.type.id).change();
                    $('#basic_form #username').val(json.data.username);
                    $('#basic_form #position').val(json.data.position);
                    $('#basic_form #email').val(json.data.email);
                    $('#basic_form #firstname').val(json.data.firstname);
                    $('#basic_form #middlename').val(json.data.middlename);
                    $('#basic_form #lastname').val(json.data.lastname);

                    $('#peronal_form #user_id').val(user_id);
                    $('#gorverment_form #user_id').val(user_id);

                    if( json.data.personal_information != null)
                    {
                        $('#peronal_form #birthday').val(json.data.personal_information.birthday);
                        $('#peronal_form #birth_place').val(json.data.personal_information.birth_place);
                        $('#peronal_form #age').val(json.data.personal_information.age);
                        $('#peronal_form #gender').val(json.data.personal_information.gender);
                        $('#peronal_form #religion').val(json.data.personal_information.religion);
                        $('#peronal_form #nationality').val(json.data.personal_information.nationality);
                        $('#peronal_form #address1').val(json.data.personal_information.address1);
                        $('#peronal_form #address2').val(json.data.personal_information.address2);
                        $('#peronal_form #address3').val(json.data.personal_information.address3);
                        $('#peronal_form #city').val(json.data.personal_information.city);
                        $('#peronal_form #region').val(json.data.personal_information.region);
                        $('#peronal_form #country').val(json.data.personal_information.country).change();
                    }
                    
                    if( json.data.goverment_information != null )
                    {
                        $('#gorverment_form #TIN').val(json.data.goverment_information.TIN);
                        $('#gorverment_form #SSS').val(json.data.goverment_information.SSS);
                        $('#gorverment_form #PAGIBIG').val(json.data.goverment_information.PAGIBIG);
                    }
                    

                },
                error: function () {
                    console.log('error');
                }
            });
    }

    var handleBasicValidation = function () {

        var form1 = $('#basic_form');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
                select_multi: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected")
                }
            },
            rules: {
                id: {
                    required: true
                },
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                var formURL = $('#basic_form').attr("action");

                $.ajax({
                  url: formURL,
                  type: 'post',
                  data: $('#basic_form').serialize(),
                  success: function(data) {
                    $('#ajax').modal('toggle');
                    if(data.result == false)
                    {
                        toastr.error(data.result, data.message);
                    }else{
                        toastr.success(data.result, data.message);
                    }
                  }
                });
                // return false; // kill page refresh
            }
        });

    }

    var handlePersonalValidation = function () {
        
        var form1 = $('#peronal_form');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
                select_multi: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected")
                }
            },
            rules: {
                id: {
                    required: true
                },
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                var formURL = $('#peronal_form').attr("action");

                $.ajax({
                  url: formURL,
                  type: 'post',
                  data: $('#peronal_form').serialize(),
                  success: function(data) {
                    $('#ajax').modal('toggle');
                    if(data.result == false)
                    {
                        toastr.error(data.result, data.message);
                    }else{
                        toastr.success(data.result, data.message);
                    }
                  }
                });
                // return false; // kill page refresh
            }
        });

    }

    var handleGovermentValidation = function () {
        var form1 = $('#gorverment_form');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
                select_multi: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected")
                }
            },
            rules: {
                id: {
                    required: true
                },
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                var formURL = $('#gorverment_form').attr("action");

                $.ajax({
                  url: formURL,
                  type: 'post',
                  data: $('#gorverment_form').serialize(),
                  success: function(data) {
                    $('#ajax').modal('toggle');
                    if(data.result == false)
                    {
                        toastr.error(data.result, data.message);
                    }else{
                        toastr.success(data.result, data.message);
                    }
                  }
                });
                // return false; // kill page refresh
            }
        });
    }

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            handleSpecialInput();
            handleUserTypeSelect();
            handleBasicValidation();
            handlePersonalValidation();
            handleGovermentValidation();

            handleDataLoad();
        }
    };
}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        AddingEmployee.init();
    });
}

